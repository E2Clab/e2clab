Changelog
=========

`3.3.1`_
-----------------------------------------------------------------------------

Fixed
~~~~~

- Major backup bug for monitoring kwollect manager

`3.3.0`_
-----------------------------------------------------------------------------

Added
~~~~~

- Added support for EnOSlib 10
- Added options ``--destroy`` for the ``finalize``  and ``deploy`` commands, allowing for automatic freeing of computing resources.
   `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#e2clab>`_
- Added a 'Kwollect' monitoring manager, allowing users to use and pull metrics from the `Grid'5000 API <https://www.grid5000.fr/w/Monitoring_Using_Kwollect>`_
   `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/monitoring/index.html#monitoring-grid-5000-using-kwollect-metrics>`_


Changed
~~~~~~~

- ``e2clab workfow SCENARIO_DIR launch --app_conf APP_CONf_LIST`` no longer runs ``finalize`` after each ``app_conf``. Use ``--finalize`` tag for this.
   `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#e2clab-workflow>`_

Fixed
~~~~~

- Loggers outputing to previous experiments files when repeating deploy experiments (#46)
- Loggers not outputing to logfiles when running workflows incrementally (#44)
- workflow_env syntax checking failing when `app_conf` names are digits (#39)

`3.2.2`_
-----------------------------------------------------------------------------

Fixed
~~~~~

- Fixed import bug that only allowed experiments containing `Iotlab` resources (#32)

`3.2.1`_
-----------------------------------------------------------------------------

Fixed
~~~~~

- Fixed `e2clab ssh` `SCENARIO_DIR` not understanding environment variables (#30)

`3.2.0`_
-----------------------------------------------------------------------------

Added
~~~~~

- Configure ``e2clab`` with environment variables like ``E2C_SCENARIO_DIR`` or ``E2C_ARTIFACTS_DIR`` to override command line arguments and default variables.
   `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#e2clab>`_
- Configure ``e2clab`` with a ``.e2c_env`` file which can be used to automatically set environment variables during command run.
   `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#e2clab>`_

- New commands:

   -  ``e2clab ssh``: Prompts the user for a host to ssh to.
      `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#e2clab-ssh>`_

   -  ``e2clab edit``: Command to quickly open an editor to scenario's configuration file.
      `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#e2clab-edit>`_

   -  ``e2clab init``: Command to quickly create ``.e2c_env`` file for your experiment.
      `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#e2clab-init>`_

   - ``e2clab get-output-dir``: Print latest experiment dir to stdout.
      `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#e2clab-get-output-dir>`_

Documentation
~~~~~~~~~~~~~

- `Enabling tab completion on your shell <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#enabling-tab-completion>`_

`3.1.0`_
-----------------------------------------------------------------------------

Changed
~~~~~~~

- [BREAKING] Service API parameter syntax change for user-defined services in `register_service` method.
   `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/services/index.html#e2clab.services.service.Service.register_service>`__


`3.0.0`_
-----------------------------------------------------------------------------

Added
~~~~~

-  New commands:

   -  ``e2clab services``: manage user-defined services without having
      to copy files around.
      `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#e2clab-services>`__
   -  ``e2clab destroy``: frees testbed resources with a single command.
      `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#e2clab-destroy>`__
   -  ``e2clab check-configuration``: checks configuration file
      syntax.\ `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#e2clab-check-configuration>`__

-  Add suppport for `EnOSlib
   9 <chttps://discovery.gitlabpages.inria.fr/enoslib/>`__
-  New ``worflow_env.yaml`` file to improve ``--app_conf`` capabilities.
   `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/exp_workflow/index.html#workflow-env-yaml-and-application-configuration>`__

   -  Allows to chain run differrent configurations of
      ``workflow.yaml``.

-  New ``e2clab.log`` and ``e2clab.err`` logging files in experiment’s
   output folders.
-  New CLI command-line options,
   `documentation <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html#e2clab>`__:

   -  ``--debug`` parameter to allow debug-level logging.
   -  ``--mute_enoslib`` mute inherited ``enoslib`` logging.
   -  ``--mute_ansible`` mute ansible spinning callback in terminal.

Changed
~~~~~~~

-  [BREAKING] Syntax verification for ``yaml`` configuration files
   enforced by jsonschemas.
   `Documentation <https://e2clab.gitlabpages.inria.fr/e2clab/define_exp_env/index.html#schemas>`__

   -  Some syntaxic changes (e.g. ``depends_on`` attribute in
      ``workflow.yaml`` must now be an array)
   -  Error logs should be verbose enough to fix configuration files.
   -  Refer to the schemas in the documentation as a baseline reference.

-  Improved logging in terminal

   -  ``e2clab.log`` and ``e2clab.err`` log files in experiment result
      dir.
   -  ``--debug`` parameter to allow debug-level logging.
   -  ``--mute_enoslib`` mute inherited ``enoslib`` logging.
   -  ``--mute_ansible`` mute ansible spinning callback in terminal.

-  [BREAKING] Changed ``optimize`` workflow.
   `Documentattion <https://e2clab.gitlabpages.inria.fr/e2clab/optimization/index.html>`__

Removed
~~~~~~~

-  [BREAKING] Retired ``optimize`` command. Instead directly use API,
   see
   `documentation <https://e2clab.gitlabpages.inria.fr/e2clab/optimization/index.html>`__

Documentation
~~~~~~~~~~~~~

-  `Experimental
   Setup <https://e2clab.gitlabpages.inria.fr/e2clab/define_exp_env/index.html>`__
-  `Experimental
   Workflow <https://e2clab.gitlabpages.inria.fr/e2clab/exp_workflow/index.html>`__
-  `E2Clab command-line
   interface <https://e2clab.gitlabpages.inria.fr/e2clab/cli-interface/index.html>`__

.. _section-1:

`2.1.0`_
-----------------------------------------------------------------------------

.. _added-1:

Added
~~~~~

-  Provenance Data Manager: allows users to capture provenance data of
   Edge-to-Cloud workflows at runtime

.. _documentation-1:

Documentation
~~~~~~~~~~~~~

-  `The Provenance
   Manager <https://e2clab.gitlabpages.inria.fr/e2clab/provenance/index.html>`__
-  `How to capture provenance data of Edge-to-Cloud
   workflows <https://e2clab.gitlabpages.inria.fr/e2clab/examples/provenance_capture.html>`__

.. _section-2:

`2.0.0`_
-----------------------------------------------------------------------------

.. _added-2:

Added
~~~~~

-  New Providers: Chameleon Cloud and CHI@Edge
-  Multi-platform workflow deployment
-  TPG monitoring: Telegraf + Promotheus + Grafana
-  FIT IoT LAB power consumption monitoring
-  New CLI commands such as ``check-testbeds`` and new parameters for
   ``deploy`` (**scenarios_name** and **app_conf**) and ``workflow``
   (**app_conf** and **finalize_wf**)
-  Add support for `EnOSlib
   8.1.5 <https://discovery.gitlabpages.inria.fr/enoslib/>`__
-  `Reusable
   User-Defined-Services <https://gitlab.inria.fr/E2Clab/user-defined-services>`__
   (+10 services)

.. _changed-1:

Changed
~~~~~~~

-  New definition of ``layers_services.yaml`` for the following
   attributes ``environment`` ( to support multiple testbeds),
   ``monitoring`` (switch between Dstat, TIG, and TPG), ``services``
   (link to different testbeds)
-  NetemHTB for network emulation

.. _documentation-2:

Documentation
~~~~~~~~~~~~~

-  `How to setup G5K, FIT IoT LAB, Chameleon Cloud and CHI@Edge
   testbeds <https://e2clab.gitlabpages.inria.fr/e2clab/environments/index.html>`__
-  `How to setup monitoring (Dstat, TIG, and
   TPG) <https://e2clab.gitlabpages.inria.fr/e2clab/monitoring/index.html>`__
-  `How to optimize workflows (workflow parameter
   search) <https://e2clab.gitlabpages.inria.fr/e2clab/optimization/index.html>`__
-  New tutorials

   -  `Edge-to-Cloud: Grid’5000 + FIT IoT
      LAB <https://e2clab.gitlabpages.inria.fr/e2clab/examples/savanna-g5k-fitiot.html>`__
   -  `Edge-to-Cloud: Chameleon Cloud +
      CHI@Edge <https://e2clab.gitlabpages.inria.fr/e2clab/examples/savanna-chameleon.html>`__
   -  `Application
      Optimization <https://e2clab.gitlabpages.inria.fr/e2clab/examples/application_optimization.html>`__
   -  `Monitoring (Dstat, TIG, and TPG) + FIT IoT LAB power
      consumption <https://e2clab.gitlabpages.inria.fr/e2clab/monitoring/index.html#try-some-examples>`__
   -  `Running COMPSs Applications on G5K (Docker
      deployment) <https://e2clab.gitlabpages.inria.fr/e2clab/examples/compss-docker.html>`__

-  `Jupyter
   Notebooks <https://www.chameleoncloud.org/experiment/share/347adbf3-7c14-4834-b802-b45fdd0d9564>`__
-  `New
   publications <https://e2clab.gitlabpages.inria.fr/e2clab/publications.html>`__

.. _section-3:

`1.0.0`_
-----------------------------------------------------------------------------

Initial public release.

.. _added-3:

Added
~~~~~

-  Allow the deployment, execution, and monitoring of application
   workflows
-  Allow the optimization of application workflows
-  Add support for EnOSlib 7.1.0
-  Add support for Ray 1.7.0

.. _3.3.1: https://gitlab.inria.fr/E2Clab/e2clab/-/tree/v3.3.1?ref_type=tags
.. _3.3.0: https://gitlab.inria.fr/E2Clab/e2clab/-/tree/v3.3.0?ref_type=tags
.. _3.2.2: https://gitlab.inria.fr/E2Clab/e2clab/-/tree/v3.2.2?ref_type=tags
.. _3.2.1: https://gitlab.inria.fr/E2Clab/e2clab/-/tree/v3.2.1?ref_type=tags
.. _3.2.0: https://gitlab.inria.fr/E2Clab/e2clab/-/tree/v3.2.0?ref_type=tags
.. _3.1.0: https://gitlab.inria.fr/E2Clab/e2clab/-/tree/v3.1.0?ref_type=tags
.. _3.0.0: https://gitlab.inria.fr/E2Clab/e2clab/-/tree/v3.0.0?ref_type=tags
.. _2.1.0: https://gitlab.inria.fr/E2Clab/e2clab/-/tree/v2.1.0?ref_type=tags
.. _2.0.0: https://gitlab.inria.fr/E2Clab/e2clab/-/tree/v2.0.0?ref_type=tags
.. _1.0.0: https://gitlab.inria.fr/E2Clab/e2clab/-/tree/v1.0.0?ref_type=tags
