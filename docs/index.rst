.. E2Clab documentation master file, created by
   sphinx-quickstart on Wed Jun 17 23:24:27 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



.. include:: HOME.rst


.. toctree::
   :maxdepth: 1
   :caption: E2Clab:


   getting-started/basics.rst
   define_exp_env/index.rst
   exp_workflow/index.rst
   cli-interface/index.rst

.. toctree::
   :maxdepth: 1
   :caption: Features:

   environments/index.rst
   services/index.rst
   services_dependencies/index.rst
   optimization/index.rst
   provenance/index.rst
   monitoring/index.rst

.. toctree::
   :maxdepth: 1
   :caption: Examples:

   examples/index.rst
   jupyter_tutorials/index.rst

.. toctree::
   :maxdepth: 1
   :caption: More:

   changelog.rst
   publications.rst
