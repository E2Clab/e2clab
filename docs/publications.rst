************
Publications
************

Main Publication
================

- Daniel Rosendo, Pedro Silva, Matthieu Simonin, Alexandru Costan, Gabriel
  Antoniu. `E2Clab: Exploring the Computing Continuum through Repeatable,
  Replicable and Reproducible Edge-to-Cloud Experiments
  <https://hal.archives-ouvertes.fr/hal-02916032>`_. Cluster 2020 - The IEEE
  2020 International Conference on Cluster Computing, Sep 2020, Kobe, Japan.

E2Clab Optimization
-------------------
- Daniel Rosendo, Alexandru Costan, Gabriel Antoniu, Matthieu Simonin,
  Jean-Christophe Lombardo, et al.. `Reproducible Performance Optimization of
  Complex Applications on the Edge-to-Cloud Continuum
  <https://hal.archives-ouvertes.fr/hal-03310540>`_. Cluster 2021 - IEEE
  International Conference on Cluster Computing, Sep 2021, Portland, OR, United
  States.

E2Clab Provenance
-----------------
- Daniel Rosendo, Marta Mattoso, Alexandru Costan, Renan Souza, Débora Pina, et al..
  `ProvLight: Efficient Workflow Provenance Capture on the Edge-to-Cloud Continuum
  <https://hal.science/hal-04161546v1>`_. Cluster 2023 - IEEE International Conference
  on Cluster Computing, Oct 2023, Santa Fe, New Mexico, United States. pp.1-13.

Other Publications (using or citing E2Clab)
===========================================

- Daniel Rosendo, Alexandru Costan, Patrick Valduriez, Gabriel Antoniu. `Distributed
  intelligence on the Edge-to-Cloud Continuum: A systematic literature review
  <https://hal.archives-ouvertes.fr/hal-03654722>`_. Journal of Parallel and Distributed
  Computing, Elsevier, 2022, 166, pp.71-94.

- Daniel Rosendo, Kate Keahey, Alexandru Costan, Matthieu Simonin, Patrick Valduriez,
  et al.. `KheOps: Cost-effective Repeatability, Reproducibility, and Replicability of
  Edge-to-Cloud Experiments <https://hal.science/hal-04157720v1>`_. ACM REP 2023 -
  Conference on Reproducibility and Replicability, Jun 2023, Santa Cruz, CA, United
  States. pp.62-73.

- Zeina Houmani, Daniel Balouek-Thomert, Eddy Caron, Manish Parashar. Enabling
  microservices management for Deep Learning applications across the Edge-Cloud Continuum.
  SBAC-PAD 2021 - IEEE 33rd International Symposium on Computer Architecture and High
  Performance Computing, Oct 2021, Belo Horizonte, Brazil. pp.1-10,
  ⟨https://hal.archives-ouvertes.fr/hal-03409405⟩.

- Daniel Balouek-Thomert, Pedro Silva, Kevin Fauvel, Alexandru Costan, Gabriel Antoniu,
  et al.. MDSC: Modelling Distributed Stream Processing across the Edge-to-Cloud Continuum.
  DML-ICC 2021 workshop (held in conjunction with UCC 2021), Dec 2021, Leicester, United
  Kingdom. ⟨https://hal.archives-ouvertes.fr/hal-03510012⟩

- Davaadorj Battulga, Daniele Miorandi, Cédric Tedeschi. SpecK: Composition of Stream
  Processing Applications over Fog Environments. DAIS 2021 - 21st International Conference
  on Distributed Applications and Interoperable Systems, Jun 2021, Valetta, Malta.
  pp.38-54, ⟨https://hal.archives-ouvertes.fr/hal-03259975⟩.

- Thomas Bouvier, Alexandru Costan, Gabriel Antoniu. Deploying Heterogeneity-aware Deep
  Learning Workloads on the Computing Continuum. BDA 2021 - 37e Conférence sur la Gestion
  de Données - Principes, Technologies et Applications, Oct 2021, Paris, France.
  ⟨https://hal.archives-ouvertes.fr/hal-03338520⟩

- Ronan-Alexandre Cherrueau, Marie Delavergne, Alexandre van Kempen, Adrien
  Lebre, Dimitri Pertin, et al.. EnosLib: A Library for Experiment-Driven Research
  in Distributed Computing. IEEE Transactions on Parallel and Distributed Systems,
  Institute of Electrical and Electronics Engineers, In press.
  ⟨https://hal.archives-ouvertes.fr/hal-03324177/⟩

- Cédric Prigent, Loïc Cudennec, Alexandru Costan, Gabriel Antoniu. A Methodology to Build
  Decision Analysis Tools Applied to Distributed Reinforcement Learning. ScaDL 2022 -
  Scalable Deep Learning over Parallel and Distributed Infrastructures - An IPDPS Workshop,
  Jun 2022, Lyon / Virtual, France. pp.1-10. ⟨https://hal.archives-ouvertes.fr/hal-03613558⟩

- Cédric Prigent, Alexandru Costan, Gabriel Antoniu, Loïc Cudennec. Enabling Federated Learning
  across the Computing Continuum: Systems, Challenges and Future Directions. Future Generation
  Computer Systems, 2024, 160, pp.767-783. <https://hal.science/hal-04659211>

- Cédric Prigent, Melvin Chelli, Alexandru Costan, Loïc Cudennec, René Schubotz, et al.. Efficient
  Resource-Constrained Federated Learning Clustering with Local Data Compression on the Edge-to-
  Cloud Continuum. HiPC 2024 - 31st IEEE International Conference on High Performance Computing,
  Data, and Analytics, Dec 2024, Bengaluru (Bangalore), India. pp.1-11. <https://hal.science/hal-04779813>

- Melvin Chelli, Cédric Prigent, René Schubotz, Alexandru Costan, Gabriel Antoniu, et al.. FedGuard:
  Selective Parameter Aggregation for Poisoning Attack Mitigation in Federated Learning. Cluster 2023
  - IEEE International Conference on Cluster Computing, Oct 2023, Santa Fe, New Mexico, United
  States. pp.1-10. <https://hal.science/hal-04208787>

- Mathis Valli, Alexandru Costan, Cédric Tedeschi, Loïc Cudennec. Towards Efficient Learning on the
  Computing Continuum: Advancing Dynamic Adaptation of Federated Learning. FlexScience 2024 -
  14th Workshop on AI and Scientific Computing at Scale using Flexible Computing Infrastructures,
  Jun 2024, Pisa, Italy. pp.42-49. <https://hal.science/hal-04698619>
