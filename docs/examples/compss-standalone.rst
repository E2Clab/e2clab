**********************************************************
Running COMPSs Applications on G5K (standalone deployment)
**********************************************************

.. contents::
   :depth: 2


This section is intended to show how to execute `COMPSs applications <https://compss-doc.readthedocs.io/en/stable/Sections/0_Intro.html>`_
on `Grid'5000 <https://www.grid5000.fr/w/Grid5000:Home>`_. This example assumes you've built a custom environment
including all COMPSs requirements (please, see `How to create a custom environment on G5K <https://www.grid5000.fr/w/Environment_creation>`_)

In this example **you will learn how to** (see :ref:`compss-deployment`):

- **Define the experimental environment**:
    - Layers and Services + Define the logic of your COMPSs Service;
    - Network;
    - Workflow (prepare, launch, finalize).
- **Deploy a standalone COMPSs Cluster: 1 Master + 3 Workers**
- **Run COMPSs applications**


.. _compss-deployment:
.. figure:: compss/standalone/compss-standalone-deployment.png
    :width: 100%
    :align: center

    Figure 1: COMPSs deployment


Experiment Artifacts
====================

.. code-block:: console

    $ git clone https://gitlab.inria.fr/E2Clab/examples/compss
    $ cd compss/

The structure of the experimental setup looks like this:

.. code-block:: none

  compss/
  ├── artifacts/          # ARTIFACTS_DIR
  │   │                   # Python scripts to generate the COMPSs configuration files
  │   ├── generate_project_xml_file.py  
  │   └── generate_resources_xml_file.py
  ├── standalone/         # SCENARIO_DIR
  │   ├── layers_services.yaml
  │   ├── network.yaml
  │   └── workflow.yaml
  └── docker/

Defining the Experimental Environment
=====================================

Defining the logic of your COMPSs Service
-----------------------------------------

This example relies on a :ref:`user-defined-service<user-defined-services>` called **COMPSs_std** which you need to import.

Clone the *user-defined-services* repository:

.. code-block:: console

    $ git clone https://gitlab.inria.fr/E2Clab/user-defined-services.git

Run this command to register this service to ``e2clab``

.. code-block:: console

    $ e2clab services add ~/user-defined-services/COMPSs_std.py --link

- `COMPSs_std <https://gitlab.inria.fr/E2Clab/user-defined-services/-/blob/master/COMPSs_std.py>`_

Here we exmplain what the `COMPSs_std <https://gitlab.inria.fr/E2Clab/user-defined-services/-/blob/master/COMPSs_std.py>`_ service does:
    - assigning machines for the **Master** and **Workers**;
    - adding information about the **Workers** to the **Master** (see ``extra=extra_compss_master`` in **COMPSs.py**)
      to generate the **resources.xml** and **project.xml** files;
    - registering the **Master** and **Worker** as a subservice of the COMPSs Service.

Layers & Services Configuration
-------------------------------

This configuration file presents the **Layers** and **Services** that compose this example. The **COMPSs_std Service** (a cluster of four nodes,
``quantity: 4``) is composed of one *Master* and three *Workers* (please, see the `COMPSs_std.py <https://gitlab.inria.fr/E2Clab/user-defined-services/-/blob/04fb4edad576d28babe24197d45a6b21269fd79a/COMPSs_std.py>`_ service definition).

We also define a **env_name** that points to a custom `grid5000` *kadeploy3* environment provided by one of the maintainers of ``e2clab`` that already contains an working ``COMPSs 3.3.1`` distribution (installed via pip). You can also use your own custom grid5000 environments by looking at the `tutorial <https://www.grid5000.fr/w/Environment_creation>`_. This option also requires a ``"deploy`` job type.

.. note::
    ``- name: COMPSs_std`` explicitely refers to the ``COMPSs_std.py`` service.


.. literalinclude:: ../../examples/compss/standalone/layers_services.yaml
   :language: yaml
   :linenos:


Network Configuration
---------------------

The file below presents the network configuration between machines in the COMPSs cluster. In this example, we defined a
constraint between the **Master** and **all Workers**.

.. literalinclude:: ../../examples/compss/standalone/network.yaml
   :language: yaml
   :linenos:


Workflow Configuration
----------------------

This configuration file presents the application workflow configuration, they are:

- Regarding **all machines in the COMPSs cluster** ``cloud.compss.*``, in ``prepare`` we compile the COMPSs application
  code and generate the .jar file.
- Regarding **just the COMPSs Master** ``cloud.compss.*.master.*``, in ``prepare`` we are copying the Python scripts to
  genetrate the **COMPSs configuration files** and then we generate such files (``resources.xml`` and ``project.xml``).
  Note that we used ``--workers {{ _self.workers }}`` since we added this information in the **COMPSs.py Service**.
  Then, in ``launch`` we **run the COMPSs application**.


.. literalinclude:: ../../examples/compss/standalone/workflow.yaml
   :language: yaml
   :linenos:


.. note::

  Besides ``prepare`` and ``launch``, you could also use ``finalize`` to backup some data (e.g., experiment results).
  E2Clab first runs on all machines the ``prepare`` tasks. Then, the ``launch`` tasks on all machines, and finally the
  ``finalize`` tasks. Regarding the ``hosts`` order, it is top to down as defined by the users in the ``workflow.yaml``
  file.


Running & Verifying Experiment Execution
========================================

Find below the command to **deploy the COMPSs cluster** on G5K and **run COMPSs applications**.

Before starting:

- make sure that your **COMPSs_std** service is correctly imported by e2clab by runing the following command: ``e2clab services list``.
- make sure that you have correctly setup your shh ``key_name`` variable as we will need to do calls to the *grid5000* API to use a custom environment.
- in the command bellow, ``compss/standalone/`` is the **scenario directory** (where the files ``layers_services.yaml``,
  ``network.yaml``, and ``workflow.yaml`` must be placed and where the results will be saved).
- in the command bellow, ``compss/artifacts/`` is the **artifacts directory** (where the Python scripts to generate the
  COMPSs configuration files must be placed).


.. code-block:: console

    $ e2clab deploy compss/standalone/ compss/artifacts/


Next, you can check the log files after the application execution.

.. code-block:: console

    # After you've ssh'd into the master host, you can find the address in 'layers_services-validate.yaml' file.
    $ cat /root/.COMPSs/hello.Hello_01/runtime.log
    $ cat /root/.COMPSs/hello.Hello_01/jobs/job1_NEW.out


Deployment Validation & Experiment Results
==========================================

Find below the files generated after the execution of the experiment. It consists of **validation files**
``layers_services-validate.yaml`` and ``workflow-validate.out``.

.. code-block:: console

    $ ls compss/standalone/<your_experiment_dir>/

    e2clab.err                      # E2Clab logs
    e2clab.log                      # E2Clab error logs
    network-validate/               # Network validation files
    layers_services-validate.yaml   # Mapping between layers and services with physical machines
    workflow-validate.out           # Commands used to deploy the application (prepare, launch, and finalize)


.. note::

  Providing a **systematic methodology to define the experimental environment** and providing **access to the methodology artifacts** (``layers_services.yaml``, ``network.yaml``, ``workflow.yaml``, and the ``COMPSs.py`` Service) leverages the experiment **Repeatability**, **Replicability**, and **Reproducibility**, see `ACM Digital Library Terminology <https://www.acm.org/publications/policies/artifact-review-badging>`_.
