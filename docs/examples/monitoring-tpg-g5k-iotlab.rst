******************************
TPG stack on G5K + FIT IoT LAB
******************************


Experiment Artifacts
====================

The artifacts repository contains the E2Clab configuration files such as
``layers_services.yaml``, ``network.yaml``, and ``workflow.yaml``

.. code-block:: console

    $ git clone https://gitlab.inria.fr/E2Clab/examples/monitoring-tpg-g5k-iotlab.git
    $ cd monitoring-tpg-g5k-iotlab/

The structure of the experimental setup looks like this:

.. code-block:: none

    monitoring-tpg-g5k-iotlab/     # SCENARIO_DIR
    ├── layers_services.yaml
    ├── network.yaml
    └── workflow.yaml

Defining the Experimental Environment
=====================================

Layers & Services Configuration
-------------------------------

The ``monitoring`` type is ``tpg`` with the ``provider`` (machine hosting Prometheus and
Grafana) on the ``g5k`` testbed (paradoxe cluster). The ``network`` is ``shared``
(experiment data and monitoring data are on the same network). We use an ``IPv6``
network (FIT IoT LAB devices use IPv6 network). Finally, we added ``roles: [monitoring]``
in all services (e.g., MyServer, MyClientA, and MyClientB) for ``tpg`` monitoring.


.. literalinclude:: ../../examples/monitoring-tpg-g5k-iotlab/layers_services.yaml
   :language: yaml
   :linenos:


Network Configuration
---------------------

.. literalinclude:: ../../examples/monitoring-tpg-g5k-iotlab/layers_services.yaml
   :language: yaml
   :linenos:


Workflow Configuration
----------------------

``prepare`` installs **stress** on all Services.

``launch`` runs **stress** on all Services.

.. literalinclude:: ../../examples/monitoring-tpg-g5k-iotlab/workflow.yaml
   :language: yaml
   :linenos:


Running & Verifying Experiment Results
======================================

Find below the commands to run this example.

.. code-block:: console

    $ e2clab layers-services ~/git/monitoring-tpg-g5k-iotlab/ ~/git/monitoring-tpg-g5k-iotlab/


.. code-block:: console

    $ e2clab workflow ~/git/monitoring-tpg-g5k-iotlab/ prepare


.. code-block:: console

    $ e2clab workflow ~/git/monitoring-tpg-g5k-iotlab/ launch



You can access the ``Grafana`` service to visualize monitoring data during experiment
execution. You can access it as described in the following file
``~/git/monitoring-tpg-g5k-iotlab/20231013-111406/layers_services-validate.yaml``. See
more details below:


.. code-block:: yaml

    '* * * * * * * Monitoring Service (started during workflow ''launch'' step)'
    'Available at: http://localhost:3000'
    'Access from your local machine: ssh -NL 3000:localhost:3000 paradoxe-4.rennes.grid5000.fr'
    'username: admin / password: admin'



.. _tpg_g5k:
.. figure:: monitoring-tpg-g5k-iotlab/figure-cpu-g5k.png
    :width: 100%
    :align: center

    Figure 1: CPU usage on G5K node 'MyServer'.


.. _tpg_a8:
.. figure:: monitoring-tpg-g5k-iotlab/figure-cpu-a8.png
    :width: 100%
    :align: center

    Figure 2: CPU usage on FIT IoT a8 device 'MyClientA'.


Wait at least one minute before ``finalizing`` the workflow and saving the monitoring data.

.. code-block:: console

    $ e2clab finalize ~/git/monitoring-tpg-g5k-iotlab/


The monitoring data will be saved at:

.. code-block:: console

    $ ls ~/git/monitoring-tpg-g5k-iotlab/20231013-111406/monitoring-data/
    20231013T094209Z-7052d33e19ef5be9.tar.gz
