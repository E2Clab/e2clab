.. _tutorials:

*********
Tutorials
*********

..
    cctv
    TO ADD after debug


.. toctree::
    :maxdepth: 2
    :caption: Contents:

    flink_click_event_count
    compss-standalone
    compss-docker
    savanna-g5k-fitiot
    savanna-chameleon
    application_optimization
    provenance_capture
    monitoring-tpg-g5k-iotlab
    monitoring-energy-dstat-g5k-iotlab
    monitoring-tig-g5k
    monitoring-tig-chameleon
    monitoring-kwollect
