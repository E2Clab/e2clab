import random
import time


def model_training(training_time):
    time.sleep(training_time)
    accuracy = round(random.uniform(0, 1), 2)
    return training_time, accuracy


if __name__ == "__main__":
    dataflow_id = "model_training"
    transformation_id = "training"
    training_input = "training_input"
    training_output = "training_output"

    # training 10x with different hyperparameters
    for training_id in range(1, 10):
        # model hyperparameters
        kernel_size = random.randint(1, 10)
        num_kernels = random.randint(8, 16)
        length_of_strides = random.randint(1, 5)
        pooling_size = random.randint(8, 16)
        # training input: model hyperparameters
        model_hyperparameters = {
            "model_hyperparameters": [
                kernel_size,
                num_kernels,
                length_of_strides,
                pooling_size,
            ]
        }
        # START training... time to train the model with hyperparameter set
        _training_time, _accuracy = model_training(training_time=random.randint(1, 5))
        # training output: model performance
        print(
            f"hyperparameters = {model_hyperparameters['model_hyperparameters']} | "
            f"accuracy = {_accuracy} / training_time = {_training_time}"
        )
