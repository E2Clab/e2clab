**********************************************
Dstat on G5K + FIT IoT LAB (power consumption)
**********************************************


Experiment Artifacts
====================

The artifacts repository contains the E2Clab configuration files such as
``layers_services.yaml``, ``network.yaml``, and ``workflow.yaml``

.. code-block:: console

    $ git clone https://gitlab.inria.fr/E2Clab/examples/monitoring-energy-dstat-g5k-iotlab.git
    $ cd monitoring-energy-dstat-g5k-iotlab/

The structure of the experimental setup looks like this:

.. code-block:: none

    monitoring-energy-dstat-g5k-iotlab/ # SCENARIO_DIR
    ├── layers_services.yaml
    ├── network.yaml
    └── workflow.yaml

Defining the Experimental Environment
=====================================

Layers & Services Configuration
-------------------------------

The ``monitoring`` type is ``dstat``. In ``monitoring_iotlab`` we defined 2 profiles to
monitor power consumption in **a8 nodes** (``name: test_capture_a8``) and **rpi3 nodes**
(``name: test_capture_rpi``).

In addition, we added ``roles: [monitoring]`` in all services (e.g., MyServer, MyClientA,
and MyClientB) for ``dstat`` monitoring. For power monitoring, we added
``profile: test_capture_a8`` in **MyClientA** and ``profile: test_capture_rpi`` in
**MyClientB**.


.. literalinclude:: ../../examples/monitoring-energy-dstat-g5k-iotlab/layers_services.yaml
   :language: yaml
   :linenos:


Network Configuration
---------------------

.. literalinclude:: ../../examples/monitoring-energy-dstat-g5k-iotlab/network.yaml
   :language: yaml
   :linenos:


Workflow Configuration
----------------------

``prepare`` installs **stress** on all Services.

``launch`` runs **stress** on all Services.

.. literalinclude:: ../../examples/monitoring-energy-dstat-g5k-iotlab/workflow.yaml
   :language: yaml
   :linenos:


Running & Verifying Experiment Results
======================================

Find below the commands to run this example.

.. code-block:: console

    $ e2clab layers-services ~/git/monitoring-energy-dstat-g5k-iotlab/ ~/git/monitoring-energy-dstat-g5k-iotlab/


.. code-block:: console

    $ e2clab workflow ~/git/monitoring-energy-dstat-g5k-iotlab/ prepare


.. code-block:: console

    $ e2clab workflow ~/git/monitoring-energy-dstat-g5k-iotlab/ launch


Wait at least one minute before ``finalizing`` the workflow and saving the monitoring data.

.. code-block:: console

    $ e2clab finalize ~/git/monitoring-energy-dstat-g5k-iotlab/


The monitoring data will be saved at:

.. code-block:: console

    $ ls ~/git/monitoring-energy-dstat-g5k-iotlab/20230927-160723/monitoring-data/
    node-a8-103.grenoble.iot-lab.info/      # dstat csv file
    node-rpi3-1.grenoble.iot-lab.info/      # dstat csv file
    paradoxe-9.rennes.grid5000.fr/        # dstat csv file


    iotlab-energy/379944-grenoble.iot-lab.info.tar.gz
        379944/consumption/rpi3_1.oml
        379944/consumption/a8_103.oml


`FIT IoT LAB <https://iot-lab.github.io/docs/tools/consumption-monitoring/>`_
provides an `OML plotting tool <https://pypi.org/project/oml-plot-tools/>`_
to help to analyse monitoring data
(`github repo <https://github.com/iot-lab/oml-plot-tools>`_).


.. code-block:: console

    $ pip install oml-plot-tools
    $ plot_oml_consum -p -c -v -i rpi3_1.oml



.. _power-rpi3_1:
.. figure:: monitoring-energy-dstat-g5k-iotlab/figure-power-rpi3_1.png
    :width: 80%
    :align: center

    Figure 1: Power consumption on rpi3 node.


.. _power-rpi3_1_stress:
.. figure:: monitoring-energy-dstat-g5k-iotlab/figure-power-rpi3_1-stress.png
    :width: 80%
    :align: center

    Figure 2: Power consumption on rpi3 node during "stress" execution for 30 seconds.
