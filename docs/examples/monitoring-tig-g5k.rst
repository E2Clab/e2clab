****************
TIG stack on G5K
****************


Experiment Artifacts
====================

The artifacts repository contains the E2Clab configuration files such as
``layers_services.yaml``, ``network.yaml``, and ``workflow.yaml``

.. code-block:: console

    $ git clone https://gitlab.inria.fr/E2Clab/examples/monitoring-tig-g5k.git
    $ cd monitoring-tig-g5k/

The structure of the experimental setup looks like this:

.. code-block:: none

    monitoring-tig-g5k/         # SCENARIO_DIR
    ├── layers_services.yaml
    ├── network.yaml
    └── workflow.yaml


Defining the Experimental Environment
=====================================

Layers & Services Configuration
-------------------------------

The ``monitoring`` type is ``tig`` with the ``provider`` (machine hosting InfluxDB and
Grafana) on the ``g5k`` testbed (paradoxe cluster). The ``network`` is ``shared``
(experiment data and monitoring data are on the same network). We use an ``IPv4``
network. Finally, we added ``roles: [monitoring]`` in all services (e.g., MyServer,
MyClientA, and MyClientB) for ``tig`` monitoring.


.. literalinclude:: ../../examples/monitoring-tig-g5k/layers_services.yaml
   :language: yaml
   :linenos:


Network Configuration
---------------------

.. literalinclude:: ../../examples/monitoring-tig-g5k/network.yaml
   :language: yaml
   :linenos:


Workflow Configuration
----------------------

``prepare`` installs **stress** on all Services.

``launch`` runs **stress** on all Services.

.. literalinclude:: ../../examples/monitoring-tig-g5k/workflow.yaml
   :language: yaml
   :linenos:


Running & Verifying Experiment Results
======================================

Find below the commands to run this example.

.. code-block:: console

    $ e2clab layers-services ~/git/monitoring-tig-g5k/ ~/git/monitoring-tig-g5k/


.. code-block:: console

    $ e2clab workflow ~/git/monitoring-tig-g5k/ prepare


.. code-block:: console

    $ e2clab workflow ~/git/monitoring-tig-g5k/ launch



You can access the ``Grafana`` service to visualize monitoring data during experiment
execution. You can access it as described in the following file
``~/git/monitoring-tig-g5k/20231013-150327/layers_services-validate.yaml``. See
more details below:


.. code-block:: yaml

    '* * * * * * * Monitoring Service (started during workflow ''launch'' step)'
    'Available at: http://localhost:3000'
    'Access from your local machine: ssh -NL 3000:localhost:3000 paradoxe-10.rennes.grid5000.fr'
    'username: admin / password: admin'


.. _tig_g5k:
.. figure:: monitoring-tig-g5k/figure-tig_g5k_cpu.png
    :width: 100%
    :align: center

    Figure 1: CPU usage on G5K node 'MyServer'.


.. _tig_a8:
.. figure:: monitoring-tig-g5k/figure-tig_g5k_mem.png
    :width: 100%
    :align: center

    Figure 2: Memory usage on G5K node 'MyServer'.


Wait at least one minute before ``finalizing`` the workflow and saving the monitoring data.

.. code-block:: console

    $ e2clab finalize ~/git/monitoring-tig-g5k/


The monitoring data will be saved at:

.. code-block:: console

    $ ls ~/git/monitoring-tig-g5k/20231013-150327/monitoring-data/
    influxdb-data.tar.gz
