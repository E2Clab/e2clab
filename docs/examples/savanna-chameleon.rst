*********************************************
Savanna Edge-to-Cloud: Chameleon Cloud + Edge
*********************************************

.. figure:: savanna-edge-to-cloud/chameleon-logo.jpg
    :width: 45%
    :align: center


.. contents::
   :depth: 2

In this tutorial, we show how to analyze the performance of a real-life Edge-to-Cloud
application deployed in the African savanna (illustrated in :ref:`xp_setup_chameleon`).

This application is composed of distributed Edge devices monitoring animal migration in
the Serengeti region. Devices at the Edge collect and compress wildlife images, then the
image is sent to the Cloud where the animal classification happens using a pre-trained
Neural Network model. Finally, classified data helps conservationists to learn what
management strategies work best to protect species.

The **goals of these experiments** are to understand the impact on performance of
`Cloud-centric` and `Hybrid (Edge+Cloud)` processing approaches.

In this example **you will learn how to**:

- Configure a cloud service on Chameleon Cloud testbed (animal classification using Deep Learning)
- Configure an edge device on Chameleon Edge testbed (collect and compress wildlife images,
  then send images to the cloud service)
- Configure the network between the edge device and cloud service.
- Execute experiments and analyze results


.. _xp_setup_chameleon:
.. figure:: savanna-edge-to-cloud/savanna-setup.png
    :width: 70%
    :align: center

    Figure 1: Edge-to-Cloud application


Experiment Artifacts
====================

.. code-block:: console

    $ cd ~/git/
    $ git clone https://gitlab.inria.fr/E2Clab/examples/savanna
    $ cd savanna/
    $ ls
    artifacts          # contains artifacts for the cloud server and the edge device
    chameleon          # contains layers_services.yaml, network.yaml, and workflow.yaml


Defining the Experimental Environment
=====================================

Layers & Services Configuration
-------------------------------

This configuration file presents the **layers** and **services** that compose this example.
The **Cloud Server** (one machine ``quantity: 1`` in Chameleon Cloud ``environment: chameleoncloud``).
The **Edge Client** (one Raspberry Pi 3 ``quantity: 1`` in Chameleon Edge ``environment: chameleonedge``).

.. literalinclude:: ../../examples/savanna/chameleon/layers_services.yaml
   :language: yaml
   :linenos:

..
    **User-Defined Service:** add the following services in the ``~/git/e2clab/e2clab/services/``
    directory.

    - `Server <https://gitlab.inria.fr/E2Clab/user-defined-services/-/blob/master/Default.py>`_
    - `Client <https://gitlab.inria.fr/E2Clab/user-defined-services/-/blob/master/Default.py>`_


.. note::

  In ``layers_services.yaml``, provide the path to your application credentials for each
  Chameleon site in ``rc_file`` and your key name in ``key_name``.


Network Configuration
---------------------

The file below presents the network configuration between the ``cloud`` and ``edge``
infrastructures ``delay: 30ms, loss: 0.1%, rate: 512kbit``.

.. literalinclude:: ../../examples/savanna/chameleon/network.yaml
   :language: yaml
   :linenos:


Workflow Configuration
----------------------

This configuration file presents the application workflow configuration.

- **The Cloud Server** ``cloud.server.*``:

``prepare`` copies from the local machine to the remote machine the artifacts

``launch`` executes the Python application (animal classification using Deep Learning)

``finalize`` after experiment ends, copies the results from the remote to the local machine

- **The Edge Client** ``edge.client.*``:

``prepare`` copies from the local machine to the remote machine the artifacts

``launch`` executes the client application (collect and compress wildlife images,
then send images to the cloud server)


.. literalinclude:: ../../examples/savanna/chameleon/workflow.yaml
   :language: yaml
   :linenos:


Understanding the parameters of **edge_worker.sh**:

- **edge_data** is the topic name
- **100** is the number of times the edge device will send images to the Cloud (every 30 seconds)
- **True** means *Edge+Cloud processing approach* (**False** means *Cloud-only processing approach*)


.. note::

  Using ``depends_on`` on the **Edge Client** ``edge.client.*`` we can access the public
  IP address to reach the **Cloud Server** as follows ``{{ server.gateway }}``.


Running & Verifying Experiment Execution
========================================

Find below the commands to deploy this application and check its execution.

.. code-block:: console

    $ e2clab deploy ~/git/savanna/chameleon/ ~/git/savanna/artifacts/

.. note::

  For the first deployment, a good practice is to do it incrementally, as `explained here
  <https://e2clab.gitlabpages.inria.fr/e2clab/getting-started/basics.html#incremental-experiment-configuration>`_.

**ssh to the Cloud server**

.. code-block:: console

  $ ssh cc@<public_ip>
  cc@<public_ip>:~$ tail -f /tmp/predict.log

.. note::

  You can find the public IP of the cloud server in the `chameleon dashboard
  <https://chi.tacc.chameleoncloud.org/project/instances/>`_

**You may also check the mosquitto topic**

You can use the Python script at ``~/git/savanna/artifacts/utils/mosquitto_sub_img.py`` to
download images received. Images will be downloaded in the directory you run the script.
In the example below, ``192.5.87.127`` is the IP address of the cloud server.

.. code-block:: console

  $ python mosquitto_sub_img.py --topic edge_data --mqtt_broker 192.5.87.127


Deployment Validation & Experiment Results
==========================================

Find below the files generated after the execution of each experiment. It consists of:

- **validation files:** ``layers_services-validate.yaml``, ``network-validate/``,
  and ``workflow-validate.out``
- **experiment resutls:** for each experiment a new directory is generated ``20230623-150252/``.

.. code-block:: console

    $ ls ~/git/savanna/chameleon/20230623-150252/

    layers_services-validate.yaml   # Mapping between layers and services with physical machines
    network-validate/               # Network configuration for each physical machine
    workflow-validate.out           # Commands used to deploy application (prepare, launch, and finalize)
    experiment-results/             # Experiment results


- **Plotting the results:** you can use the ``~/git/savanna/artifacts/utils/plot_processing-time.py``
  Python script to plot the results as in :ref:`plot_chameleon`. Update the script
  according to your needs.


.. _plot_chameleon:
.. figure:: ./savanna-edge-to-cloud/chameleoncloud_chameleonedge/plot-chameleon.png
    :width: 80%
    :align: center

    Figure 2:  Cloud-centric vs Edge+Cloud processing
