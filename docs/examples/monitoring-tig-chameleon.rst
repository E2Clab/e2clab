**********************
TIG stack on Chameleon
**********************


Experiment Artifacts
====================

The artifacts repository contains the E2Clab configuration files such as
``layers_services.yaml``, ``network.yaml``, and ``workflow.yaml``

.. code-block:: bash

    cd ~/git/
    git clone https://gitlab.inria.fr/E2Clab/examples/monitoring-tig-chameleon.git
    cd monitoring-tig-chameleon/


Defining the Experimental Environment
=====================================

Layers & Services Configuration
-------------------------------

The ``monitoring`` type is ``tig`` with the ``provider`` (machine hosting InfluxDB and
Grafana) on the ``chameleoncloud`` testbed (compute_cascadelake_r cluster). Finally, we
added ``roles: [monitoring]`` in all services (e.g., Server and Client) for ``tig``
monitoring.


.. literalinclude:: ../../examples/monitoring-tig-chameleon/layers_services.yaml
   :language: yaml
   :linenos:


Network Configuration
---------------------

.. literalinclude:: ../../examples/monitoring-tig-chameleon/network.yaml
   :language: yaml
   :linenos:



Workflow Configuration
----------------------

``prepare`` installs **stress** on all Services.

``launch`` runs **stress** on all Services.

.. literalinclude:: ../../examples/monitoring-tig-chameleon/workflow.yaml
   :language: yaml
   :linenos:


Running & Verifying Experiment Results
======================================

Find below the commands to run this example.

.. code-block:: console

    $ e2clab layers-services ~/git/monitoring-tig-chameleon/ ~/git/monitoring-tig-chameleon/


.. code-block:: console

    $ e2clab workflow ~/git/monitoring-tig-chameleon/ prepare


.. code-block:: console

    $ e2clab workflow ~/git/monitoring-tig-chameleon/ launch



You can access the ``Grafana`` service to visualize monitoring data during experiment
execution. You can access it as described in the following file
``~/git/monitoring-tig-chameleon/20231016-105832/layers_services-validate.yaml``. See
more details below:

.. code-block:: yaml

    '* * * * * * * Monitoring Service (started during workflow ''launch'' step)'
    'Available at: http://localhost:3000'
    'Access from your local machine: ssh -NL 3000:localhost:3000 cc@<FLOATING_IP>'
    'username: admin / password: admin'


.. note::

    - Allocate a `floating IP` (public IP) using Chameleon's dashboard. See Figure 1.
    - Assign the IP to the monitoring service. See Figures 2 and 3.

.. _tig_chameleoncloud_allocate_ip:
.. figure:: monitoring-tig-chameleon/figure-allocate_ip.png
    :width: 100%
    :align: center

    Figure 1: Allocating a floating IP (public IP) using Chameleon's dashboard.


.. _tig_chameleoncloud_assign_ip:
.. figure:: monitoring-tig-chameleon/figure-assign_ip.png
    :width: 100%
    :align: center

    Figure 2: Assigning public IP to the monitoring service.


.. _tig_chameleoncloud_allocateed_ip:
.. figure:: monitoring-tig-chameleon/figure-allocated_ip.png
    :width: 100%
    :align: center

    Figure 3: Chameleon cloud node with IP address allocated.

.. _tig_chameleoncloud_cpu:
.. figure:: monitoring-tig-chameleon/figure-tig_chameleoncloud_cpu.png
    :width: 100%
    :align: center

    Figure 4: CPU usage on Chameleon cloud node 'Server'.


Wait at least one minute before ``finalizing`` the workflow and saving the monitoring data.

.. code-block:: console

    $ e2clab finalize ~/git/monitoring-tig-chameleon/


The monitoring data will be saved at:

.. code-block:: console

    $ ls ~/git/monitoring-tig-chameleon/20231016-105832/monitoring-data/
    influxdb-data.tar.gz
