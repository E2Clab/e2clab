.. _e2clab-cli:

*****************************
E2Clab command-line interface
*****************************

.. contents::
   :depth: 2

Getting help
============

You can also get all the following information by running the following command:

.. code-block:: bash

   e2clab --help

E2Clab Command Line Interface
=============================

The commands allowed in E2Clab are presented below. Note that you can use just a single
command to deploy your application ``deploy`` or you can deploy it separately using the
commands ``layers-services``, ``network``, ``workflow`` (``prepare``, ``launch``, or
``finalize``), and ``finalize``, respectively.

.. click:: e2clab.cli:cli
  :prog: e2clab
  :nested: full

Enabling Tab Completion
=======================

As `e2clab` relies on the `Click Python package <https://click.palletsprojects.com>`_ it provides tab completion for `Bash` `Zsh` and `Fish` shells.

From the `documentation <https://click.palletsprojects.com/shell-completion/>`_, to enable tab completion you may follow the following procedure:

.. tabs::

    .. group-tab:: Bash

        Save the script somewhere.

        .. code-block:: bash

            _E2CLAB_COMPLETE=bash_source e2clab > ~/.e2clab-complete.bash

        Source the file in ``~/.bashrc``.

        .. code-block:: bash

            . ~/.e2clab-complete.bash

    .. group-tab:: Zsh

        Save the script somewhere.

        .. code-block:: bash

            _E2CLAB_COMPLETE=zsh_source e2clab > ~/.e2clab-complete.zsh

        Source the file in ``~/.zshrc``.

        .. code-block:: bash

            . ~/.e2clab-complete.zsh

    .. group-tab:: Fish

        Save the script to ``~/.config/fish/completions/e2clab.fish``:

        .. code-block:: fish

            _E2CLAB_COMPLETE=fish_source e2clab > ~/.config/fish/completions/e2clab.fish
