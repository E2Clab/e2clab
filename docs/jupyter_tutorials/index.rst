*****************
Jupyter Tutorials
*****************



.. figure:: ./trovi-jupyter-e2clab.png
    :width: 70%
    :align: center


.. contents::
   :depth: 2


Context
=======

In this tutorial, we show how to analyze the performance of a real-life Edge-to-Cloud
application deployed in the African savanna (illustrated in :ref:`xp_setup_jup`).

This application is composed of distributed Edge devices monitoring animal migration in
the Serengeti region. Devices at the Edge collect and compress wildlife images, then the
image is sent to the Cloud where the animal classification happens using a pre-trained
Neural Network model. Finally, classified data helps conservationists to learn what
management strategies work best to protect species.

The **goals of these experiments** are to understand the impact on performance of
`Cloud-centric` and `Hybrid (Edge+Cloud)` processing approaches.

In this example **you will learn how to**:

- Configure a cloud server on (Grid'5000 or Chameleon Cloud) testbed (animal classification
  using Deep Learning)
- Configure an edge device on (FIT IoT LAB or Chameleon Edge) testbed (collect and compress
  wildlife images, then send images to the cloud server)
- Configure the network between the edge device and cloud server.
- Execute experiments and analyze results


.. _xp_setup_jup:
.. figure:: ../examples/savanna-edge-to-cloud/savanna-setup.png
    :width: 70%
    :align: center

    Figure 1: Edge-to-Cloud application


Executing experiments
=====================

You can execute the experiments as follows:

- Access the experiment artifacts available in the `Trovi sharing portal
  <https://www.chameleoncloud.org/experiment/share/347adbf3-7c14-4834-b802-b45fdd0d9564>`_
- Then, launch the Jupyter Notebook by clicking on ``Launch on Chameleon`` button.
- Finally, execute experiments by following the instructions in the ``Welcome.ipynb`` and
  then ``chameleon_edge_to_cloud.ipynb`` (if you deploy on Chameleon Cloud + Edge testbeds)
  or ``iotlab_to_g5k_cloud.ipynb`` (if you deploy on Grid'5000 + FIT IoT LAB testbeds)

.. note::

    If you don't have a Chameleon account, you can request a
    `Chameleon Daypass <https://chameleoncloud.org/blog/2022/01/24/interactive-science-made-easy-with-chameleon-daypass/>`_
