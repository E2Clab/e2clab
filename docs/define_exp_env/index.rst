******************
Experimental setup
******************

.. _experimental_setup:

.. toctree::
    :maxdepth: 2
    :caption: Contents:

E2Clab's experimental setup follows a 3-step methodology.

.. figure:: ../figures/E2Clab-methodology.png
    :width: 70%
    :align: center

    Figure 1: E2Clab experiment methodology.

- ``Layers & Services``
    - **Services** represent any system that provides a specific functionality or action in the scenario workflow.
    - **Layers** define the hierarchy between services and groups them with different granularities. They can also be used to reflect the geographical distribution of the compute resources. In the context of the Computing Continuum, *layers* refers to Edge, Fog and Cloud for instance.
- ``Network``
    - Specifies the network communication rules between **layers** and between **services**
- ``Workflow``
    - Specifies all the execution logic and rules of the software, algorithms and applications running on **services**.

Those 3 steps are respectively defined in the following ``YAML`` files :

- :ref:`layers_services.yaml<layers_services>`
- :ref:`network.yaml<network>`
- :ref:`workflow.yaml<workflow>`

Each of these configuration files will be parsed by ``e2clab`` and used by the respective *manager* within ``e2clab``.

Those three files must be present in the same folder, which later in the documentation will be refered to as ``SCENARIO_DIR`` or *scenario*.

.. code-block:: none

    SCENARIO_DIR/
    ├─layers_services.yaml
    ├─network.yaml
    ├─workflow.yaml
    └─...

To know more about ``YAML`` files and their syntax, you can check `Ansible's YAML Syntax guide`_.

Schemas
=======

JSON schemas (`jsonschema documentation`_) are utilized within ``e2clab`` to validate the syntax and structure of the YAML configuration files.
They are a powerful way to ensure that those YAML files adhere to the expected format.

You don't need to understand them but you can use them as a reference for the ``YAML`` files syntax and for parameter meanings and possible values.

- :ref:`layers-services jsonschema<layers_services_schema>`
- :ref:`network jsonschema<network_schema>`
- :ref:`workflow jsonschema<workflow_schema>`

.. _layers_services:

Layers & Services
=================

The layers and services of the experiment are defined in a ``layers_services.yaml`` file. In the computing-continuum field, those layers are usually called 'cloud', 'fog' and 'edge' each hosting several services.

Environments
------------

The ``environment`` section of our ``layers_services.yaml`` file describes the testbeds used to run our ``e2clab`` experiment on. Single-testbed or mumtiple testbeds experiments are possible on the available testbeds :

- :ref:`Grid5000<setup_g5k>`
- :ref:`FIT IoT-LAB<setup_iotlab>`
- :ref:`Chameleon Cloud and Edge<setup_chameleon>`

For a multi-testbed development setup, you can check :ref:`this example<savanna_g5k_fit>`.

Layers
------

The ``layers`` section of our ``layers_services.yaml`` file describes the hierarchy between services and groups them.

Services
--------

To know more about how services work in ``e2clab`` and how you can define you own services, check :ref:`the documentation<user-defined-services>`.

.. By default, ``e2clab`` comes with a *Default* *e2clab* service which is bascically a bare node from your provider according to how you setup your environment.

If you have :ref:`user-defined services<user-defined-services>` installed that you wish to use in your Layer & Services definiton, the name of your service in the ``layers_services.yaml`` must match (case-sensitive) the name of your user-defined service.
If the name of your service in your ``layers_services.yaml`` file doesn't match any user-defined service, a *Default* service will be deployed, meaning a bare-metal node from your configured testbed.

You can deploy services on different testbeds in a same experiment (``grid5000`` for cloud services and ``FIT IoT-LAB`` for edge services for example).
:ref:`Check the documentation<service_deploy>` to see how to deploy a user-defined service and how to pass metadata to your services.

Monitoring
----------

:ref:`Check the monitoring documentation<monitoring_doc>`

Provenance
----------

:ref:`Check the provenance documentation<provenance_doc>`

Syntax
------

Here is a dummy ``layers_services.yaml`` file for a ``e2clab`` experiment exposing several configuration parameters and their meaning.
For more information on each parameter, check :ref:`the schema definition<layers_services_schema>`.

.. literalinclude:: ./layers_services.yaml
    :language: yaml
    :linenos:

.. _layers_services_schema:

Schema
------

.. dropdown:: configuration jsonschema

    .. jsonschema:: e2clab.schemas.layers_services_schema.SCHEMA
        :lift_definitions:
        :auto_reference:
        :auto_target:

.. _network:

Network
=======

``e2clab`` has the capacity to emulate network constraints between layers, or any subset of services. Those constraints are defined in a single ``network.yaml`` configuration file.
For each network, users may set parameters like:

- delay
- rate
- loss

This is a usefull functionality to simulate lossy networks between edge and cloud services for example.

This network emulation relies on EnOSlib's `network emulation service`_.

Syntax
------

Here is a dummy ``network.yaml`` file for a ``e2clab`` experiment.
For more information on each parameter, check :ref:`the schema definition<network_schema>`.

.. literalinclude:: ./network.yaml
    :language: yaml
    :linenos:

The network topology and emulation is defined in the ``network.yaml`` file.


.. _network_schema:

Schema
------


.. dropdown:: configuration jsonschema

    .. jsonschema:: e2clab.schemas.network_schema.SCHEMA
        :lift_definitions:
        :auto_reference:
        :auto_target:

.. _workflow:

Workflow
========

The workflow of the experiment (i.e. the orchestration of your **application**) is defined in the ``workflow.yaml`` file. `e2clab` relies heavily on `ansible` to enforce commands on the remote hosts.

Hosts
-----

Hosts refer to remote hosts used to deploy the experiment services (libraries, applications...) and to enforce the workflow.
They can be :ref:`selected<host_select>` according to their :ref:`tags<service_tags>`. You can also find those tags in the :ref:`layers_services-validate` file.

Depends on
----------

The *depends-on* attribute allows users to map services in a reproducible and scalable way.
The mapping strategy is described by the *grouping* type.
To know more about how those mapping strategies work and how services can access other services parameters (such as a *url*), check the :ref:`services relationships documentation<depends_on>`

Deployment
----------

Users can manage the deployment of their application in three well defined steps

- **prepare**: install software, dependencies or populate hosts with the experiment's input .
- **launch**: launch experiment.
- **finalize**: stop experiment, clean environment and fetch experiment artifacts from the hosts.

Each of those steps are an **ansible** task definition like you would in a playbook, so if you are already familiar with the syntax this should be easy.

.. _workflow_variables:

Variables
---------

Several variables are exposed by ``e2clab`` inside your ``workflow.yaml`` file to help you manage you experiment:

- ``scenario_dir``: points to ``SCENARIO_DIR`` passed in the command line
- ``artifacts_dir``: points to ``ARTIFACTS_DIR`` passed in the command line, usefull to manade input data and load hosts with scripts.
- ``working_dir``:
    - points to ``SCENARIO_DIR`` during ``prepare`` and ``launch`` steps
    - points to your experiment's output dir during ``finalize`` step. Helpfull to backup output data.
- ``app_conf``: see :ref:`this section<app_conf>`

Other variables exposing services to each-other are available, check :ref:`how to access variables from other services<relationships>` and how to :ref:`access service's own variables<service_self>`.

Syntax
------

Here is an example ``workflow.yaml`` file for a ``e2clab`` experiment. For more information on each parameter, check :ref:`the schema definition<workflow_schema>`.

.. literalinclude:: ./workflow.yaml
    :language: yaml
    :linenos:


.. _workflow_schema:

Schema
------

.. dropdown:: configuration jsonschema

    .. jsonschema:: e2clab.schemas.workflow_schema.SCHEMA
        :lift_definitions:
        :auto_reference:
        :auto_target:

.. _Ansible's YAML Syntax guide: https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html
.. _jsonschema documentation: https://json-schema.org/
.. _network emulation service: https://discovery.gitlabpages.inria.fr/enoslib/apidoc/netem.html
