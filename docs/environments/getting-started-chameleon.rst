************************
Chameleon Cloud and Edge
************************

.. contents::
   :depth: 2


.. _setup_chameleon:

Setting up Chameleon platform
=============================

Application Credential
----------------------

`Create you application credential <https://chameleoncloud.readthedocs.io/en/latest/technical/cli.html?highlight=OPENRC%20file#creating-an-application-credential>`_
and download it. Do it for each site you plan to use (e.g.,
`CHI@TACC <https://chi.tacc.chameleoncloud.org/project/>`_,
`CHI@UC <https://chi.uc.chameleoncloud.org/project/>`_,
`CHI@Edge <https://chi.edge.chameleoncloud.org/>`_, among others).

.. note::

  Once the system generates the credential, you will be given the option to download an
  RC file that configures the CLI to use the application credential for authentication.
  You will only see the secret credentials once, so make sure to save the RC file or the
  secret somewhere, as if it’s lost, you will have to delete the credential and create a
  new one.

Then, add the references to these files in ``layers_services.yaml`` as follows (example
with Chameleon Cloud TACC site and Chameleon Edge):


SSH External access
-------------------

Add the public ssh key (your workstation: ``~/.ssh/id_rsa.pub``) to your Chameleon cloud
sites as follows:
`Create or import a key pair <https://chameleoncloud.readthedocs.io/en/latest/technical/gui.html?highlight=OPENRC%20file#key-pairs>`_
for Chameleon Cloud sites (e.g.,
`CHI@TACC <https://chi.tacc.chameleoncloud.org/project/>`_,
`CHI@UC <https://chi.uc.chameleoncloud.org/project/>`_, etc.).

Then, add the ``Key pair name`` in ``layers_services.yaml``.


.. literalinclude:: layers_services_chameleon.yaml
   :language: yaml
   :linenos:
