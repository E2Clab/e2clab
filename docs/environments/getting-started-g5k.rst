*********
Grid'5000
*********

.. contents::
   :depth: 2


.. _setup_g5k:

Setting up Grid'5000 platform
=============================

SSH External access
-------------------

Add the public ssh key (e.g. on your workstation: ``~/.ssh/id_rsa.pub``) to your
`Grid'5000 account <https://api.grid5000.fr/stable/users/>`_.

In order to allow you to ssh from your workstation to a G5K frontend (e.g.,
ssh rennes.grid5000.fr), configure ``~/.ssh/config`` (in your workstation) as follows:

.. code-block:: bash

    Host !access.grid5000.fr *.grid5000.fr
        User <login>
        ProxyJump <login>@access.grid5000.fr
        StrictHostKeyChecking no
        UserKnownHostsFile /dev/null
        ForwardAgent yes

        Host access.grid5000.fr
        User <login>
        StrictHostKeyChecking no
        UserKnownHostsFile /dev/null
        ForwardAgent yes


.. note::

    Replace ``<login>`` by your G5K login.


Try to access a G5K frontend:

.. code-block:: bash

    # from your workstation
    $ ssh rennes.grid5000.fr


REST API
========

When accessing the API from outside of Grid’5000 (e.g. your workstation), you need to
specify the following configuration file:

.. code-block:: bash

    echo '
    username: MYLOGIN
    password: MYPASSWORD
    ' > ~/.python-grid5000.yaml

    chmod 600 ~/.python-grid5000.yaml

**When accessing the API from a Grid’5000 frontend**:
- Providing the ``username`` and ``password`` is optional;
- Nevertheless you’ll need to deal with SSL verification by specifying the path to the
certificate to use:

.. code-block:: bash

    echo '
    verify_ssl: /etc/ssl/certs/ca-certificates.crt
    ' > ~/.python-grid5000.yaml
