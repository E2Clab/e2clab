***********
FIT IoT LAB
***********

.. contents::
   :depth: 2


.. _setup_iotlab:

Setting up FIT IoT LAB platform
===============================

SSH External access
-------------------

Add the public ssh key (your workstation: ``~/.ssh/id_rsa.pub``) to your
`FIT IoT LAB account <https://www.iot-lab.info/testbed/account>`_.

**Create the credentials file:**

- replace ``<fit_login>`` by your FIT IoT LAB username
- replace ``<fit_password>`` by your FIT IoT LAB password

.. code-block:: bash

    <fit_login>:$(echo -n <fit_password> | base64) > ~/.iotlabrc


In order to allow you to ssh from your workstation to a FIT IoT LAB frontend (e.g.,
ssh <fit_login>@<frontend>.iot-lab.info), configure ``~/.ssh/config`` (in your workstation)
as follows:

.. code-block:: bash

    Host !grenoble.iot-lab.info *.grenoble.iot-lab.info
       User <fit_login>
       ProxyJump <fit_login>@grenoble.iot-lab.info
       StrictHostKeyChecking no
       UserKnownHostsFile /dev/null
       ForwardAgent yes

       Host grenoble.iot-lab.info
       User <fit_login>
       StrictHostKeyChecking no
       UserKnownHostsFile /dev/null
       ForwardAgent yes

    Host !saclay.iot-lab.info *.saclay.iot-lab.info
       User <fit_login>
       ProxyJump <fit_login>@saclay.iot-lab.info
       StrictHostKeyChecking no
       UserKnownHostsFile /dev/null
       ForwardAgent yes

       Host saclay.iot-lab.info
       User <fit_login>
       StrictHostKeyChecking no
       UserKnownHostsFile /dev/null
       ForwardAgent yes

.. note::

    Replace ``<fit_login>`` by your FIT IoT LAB login. Example with ``grenoble`` and
    ``saclay`` sites.


Try to access a FIT IoT LAB frontend in ``grenoble``:

.. code-block:: bash

    # from your workstation
    $ ssh <fit_login>@grenoble.iot-lab.info
