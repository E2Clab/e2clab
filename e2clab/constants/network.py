"""Network conf file constants"""

NETWORKS = "networks"
SRC = "src"
DST = "dst"
DELAY = "delay"
RATE = "rate"
LOSS = "loss"
SYMMETRIC = "symmetric"
