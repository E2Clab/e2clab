"""
Testing e2clab.infra module
"""

from copy import deepcopy
from pathlib import Path
from typing import TextIO
from unittest.mock import Mock, call, create_autospec, patch

from enoslib import DefaultNetwork, Host, Networks, Roles

import e2clab.infra as e2cinfra
from e2clab.config import InfrastructureConfig
from e2clab.constants import ConfFiles, Environment
from e2clab.constants.layers_services import (
    ARCHI,
    CLUSTER,
    DEFAULT_SERVICE_NAME,
    ENVIRONMENT,
    ID,
    JOB_NAME,
    LAYERS,
    MONITORING_IOT_AVERAGE,
    MONITORING_IOT_PERIOD,
    MONITORING_IOT_PROFILES,
    MONITORING_IOT_SVC,
    MONITORING_SVC,
    MONITORING_SVC_PROVIDER,
    MONITORING_SVC_TIG,
    MONITORING_SVC_TYPE,
    NAME,
    PROVENANCE_SVC,
    PROVENANCE_SVC_DATAFLOW_SPEC,
    PROVENANCE_SVC_PROVIDER,
    REPEAT,
    SERVICES,
    WALLTIME,
)
from e2clab.experiment import E2clabError
from e2clab.managers import Manager, Managers
from e2clab.providers.plugins.G5k import G5k
from e2clab.providers.plugins.Iotlab import Iotlab
from e2clab.services.plugins.Default import Default
from e2clab.tests.unit import TestE2cLab


class TestInfra(TestE2cLab):
    """
    Testing Infra class
    """

    @classmethod
    def setUpClass(cls):
        df_spec = "dataflow_spec_file"
        cls.multiprov_valid_conf = {
            ENVIRONMENT: {
                JOB_NAME: "test",
                WALLTIME: "01:00:00",
                Environment.G5K.value: {CLUSTER: "paradoxe"},
                Environment.IOT_LAB.value: {CLUSTER: "grenoble"},
            },
            LAYERS: [
                {
                    NAME: "cloud",
                    SERVICES: [
                        {NAME: "Exp", ENVIRONMENT: Environment.G5K.value, REPEAT: 3},
                        {NAME: "Serv"},
                    ],
                },
                {
                    NAME: "fog",
                    SERVICES: [
                        {
                            NAME: "Fog",
                            ENVIRONMENT: Environment.CHAMELEON_CLOUD.value,
                            REPEAT: 3,
                        }
                    ],
                },
                {
                    NAME: "edge",
                    SERVICES: [
                        {
                            NAME: "Prod",
                            ENVIRONMENT: Environment.IOT_LAB.value,
                            REPEAT: 1,
                        }
                    ],
                },
            ],
            PROVENANCE_SVC: {
                PROVENANCE_SVC_PROVIDER: Environment.G5K.value,
                CLUSTER: "paradoxe",
                PROVENANCE_SVC_DATAFLOW_SPEC: df_spec,
            },
            MONITORING_SVC: {
                MONITORING_SVC_TYPE: MONITORING_SVC_TIG,
                MONITORING_SVC_PROVIDER: Environment.G5K.value,
            },
            MONITORING_IOT_SVC: {
                MONITORING_IOT_PROFILES: [
                    {
                        NAME: "Test",
                        ARCHI: "a8",
                        MONITORING_IOT_AVERAGE: 1024,
                        MONITORING_IOT_PERIOD: 8244,
                    }
                ]
            },
        }

    def setUp(self) -> None:
        self.config = self.test_folder / ConfFiles.LAYERS_SERVICES
        self.infra = e2cinfra.Infrastructure(
            config=self.config,
            optimization_id=None,
        )

    def test_infra_init(self):
        self.assertIsInstance(self.infra, e2cinfra.Infrastructure)
        self.assertIsNone(self.infra.optimization_id)

    def test_infra_prepare(self):
        self.infra.prepare()

        # true if no already imported services
        self.assertEqual(self.infra.prov_to_load, [Environment.G5K])
        self.assertIn(DEFAULT_SERVICE_NAME, self.infra.serv_to_load)

    @patch.object(e2cinfra.Infrastructure, "_load_providers")
    @patch.object(e2cinfra.Infrastructure, "_create_providers")
    @patch.object(e2cinfra.Infrastructure, "_init_providers_merge_resources")
    @patch.object(e2cinfra.Infrastructure, "_load_services")
    @patch.object(e2cinfra.Infrastructure, "_create_services")
    def test_infra_deploy(
        self,
        m_create_services: Mock,
        m_load_services: Mock,
        m_init_providers_merge: Mock,
        m_create_providers: Mock,
        m_load_providers: Mock,
    ):
        test_loaded_providers = {Environment.G5K: G5k, Environment.IOT_LAB: Iotlab}
        m_load_providers.return_value = test_loaded_providers

        test_loaded_services = {"Default": Default}
        m_load_services.return_value = test_loaded_services

        test_providers = {Environment.G5K: Mock()}
        m_create_providers.return_value = test_providers

        m_init_providers_merge.return_value = (Roles(), Networks())

        m_monitor_iot_manager = Mock(spec=Managers.MONITORING_IOT.value)
        m_monitor_manager = Mock(spec=Managers.MONITORING.value)
        m_provenance_manager = Mock(spec=Managers.PROVENANCE.value)

        self.infra.managers = {
            Managers.MONITORING_IOT: m_monitor_iot_manager,
            Managers.MONITORING: m_monitor_manager,
            Managers.PROVENANCE: m_provenance_manager,
        }
        for e_mock, mock in self.infra.managers.items():
            mock.get_extra_info.return_value = {e_mock.name: "test"}
            mock.get_environment.return_value = Environment.G5K

        roles, networks = self.infra.deploy(self.test_folder, "/tmp")

        m_load_providers.assert_called_once_with()
        m_create_providers.assert_called_once_with(test_loaded_providers)
        m_init_providers_merge.assert_called_once_with()
        m_load_services.assert_called_once_with()
        m_create_services.assert_called_once_with(test_loaded_services)

        self.assertEqual(roles, Roles())
        self.assertEqual(networks, Networks())
        # Check extrainformation update
        self.assertIn("MONITORING", self.infra.all_serv_extra_inf)
        self.assertIn("PROVENANCE", self.infra.all_serv_extra_inf)
        self.assertIn("MONITORING_IOT", self.infra.all_serv_extra_inf)

        # Test failed manager provider setup:
        m_monitor_manager.get_environment.return_value = Environment.CHAMELEON_CLOUD
        with self.assertRaises(E2clabError):
            self.infra.deploy(self.test_folder, "/tmp")

    @patch.object(Path, "mkdir")
    def test_finalize(self, m_path: Mock):
        m_monitor_iot_manager = Mock(spec=Managers.MONITORING_IOT.value)
        m_monitor_manager = Mock(spec=Managers.MONITORING.value)
        m_provenance_manager = Mock(spec=Managers.PROVENANCE.value)

        self.infra.managers = {
            Managers.MONITORING_IOT: m_monitor_iot_manager,
            Managers.MONITORING: m_monitor_manager,
            Managers.PROVENANCE: m_provenance_manager,
        }

        test_output_dir = Path("/tmp")

        self.infra.finalize(test_output_dir)
        for manager in self.infra.managers.values():
            manager.backup.assert_called_once_with(output_dir=test_output_dir)
            manager.destroy.assert_called_once_with()

    def test_load_provider(self):
        self.infra.prepare()

        loaded_providers = self.infra._load_providers()
        self.assertIn(Environment.G5K, loaded_providers.keys())
        self.assertEqual(len(loaded_providers), 1)

    def test_create_providers(self):
        self.infra.prepare()

        loaded_providers = {Environment.G5K: G5k}

        providers = self.infra._create_providers(loaded_providers)

        self.assertEqual(len(providers.keys()), 1)
        self.assertIsInstance(providers[Environment.G5K], G5k)

    def test_load_services(self):
        self.infra.serv_to_load = [DEFAULT_SERVICE_NAME]
        services = self.infra._load_services()

        self.assertEqual(len(services.keys()), 1)
        self.assertEqual(services[DEFAULT_SERVICE_NAME], Default)

    @patch.object(Iotlab, "destroy")
    @patch.object(G5k, "destroy")
    def test_infra_destroy(self, mock_g5k_destroy: Mock, mock_iotlab_destroy: Mock):
        self.infra.providers = {
            Environment.G5K: G5k(self.infra.config, None),
            Environment.IOT_LAB: Iotlab(self.infra.config, None),
        }
        self.infra.destroy()
        mock_g5k_destroy.assert_called_once()
        mock_iotlab_destroy.assert_called_once()

    @patch.object(Iotlab, "init")
    @patch.object(G5k, "init")
    def test_init_providers_merge_resources(
        self, m_g5k_init: Mock, m_iotlab_init: Mock
    ):
        g5k_host = Host("1.1.1.1")
        g5k_net = DefaultNetwork("10.0.0.1/24")
        m_g5k_init.return_value = (
            Roles(G5K_role=[g5k_host]),
            Networks(G5K_role=[g5k_net]),
        )
        iot_host = Host("9.9.9.9")
        iot_net = DefaultNetwork("11.0.0.1/24")
        m_iotlab_init.return_value = (
            Roles(IOTLAB_role=[iot_host]),
            Networks(IOTLAB_role=[iot_net]),
        )

        self.infra.config = InfrastructureConfig(self.multiprov_valid_conf)
        self.infra.providers = {
            Environment.G5K: G5k(deepcopy(self.infra.config), None),
            Environment.IOT_LAB: Iotlab(deepcopy(self.infra.config), None),
        }
        roles, networks = self.infra._init_providers_merge_resources()

        m_g5k_init.assert_called_once_with()
        m_iotlab_init.assert_called_once_with()

        self.assertIn("G5k", roles)
        self.assertCountEqual(roles["G5k"], [g5k_host])
        self.assertCountEqual(roles["G5K_role"], [g5k_host])
        self.assertIn("Iotlab", roles)
        self.assertCountEqual(roles["Iotlab"], [iot_host])
        self.assertCountEqual(roles["IOTLAB_role"], [iot_host])
        self.assertIn("G5k", networks)
        self.assertCountEqual(networks["G5k"], [g5k_net])
        self.assertCountEqual(networks["G5K_role"], [g5k_net])
        self.assertIn("Iotlab", networks)
        self.assertCountEqual(networks["Iotlab"], [iot_net])
        self.assertCountEqual(networks["IOTLAB_role"], [iot_net])

    @patch("e2clab.config.get_available_services")
    def test_create_services(self, m_available_services: Mock):
        m_available_services.return_value = ["Default", "Kafka", "Flink"]
        # Have to run this to get SERVICE_PLUGIN_NAME
        services_to_load = self.infra.config.get_services_to_load()
        m_available_services.assert_called_once()
        self.assertCountEqual(services_to_load, ["Default", "Kafka", "Flink"])

        # Failed run, failed to import services
        with self.assertRaises(E2clabError):
            self.infra._create_services({})

        self.infra.roles = Roles(
            {
                "1_1": [Host("1.1.1.1")],
                "1_2": [Host("2.2.2.2")],
                "3_1": [Host("3.3.3.3")],
            }
        )

        mock_default = Mock(spec=Default)
        mock_kafka = Mock(spec=Default)
        mock_flink = Mock(spec=Default)
        loaded_services = {
            "Default": mock_default,
            "Kafka": mock_kafka,
            "Flink": mock_flink,
        }
        for name, mock in loaded_services.items():
            mock.return_value._init.return_value = ({name: {"test": 123}}, Roles())
        self.infra._create_services(loaded_services)

        for name, mock in loaded_services.items():
            mock.assert_called()
            mock.return_value._init.assert_called()
            self.assertIn(name, self.infra.all_serv_extra_inf)

    def test_filtered_user_roles(self):
        config = {
            ENVIRONMENT: {
                JOB_NAME: "test",
                WALLTIME: "01:00:00",
                Environment.G5K.value: {CLUSTER: "paradoxe"},
            },
            LAYERS: [
                {
                    NAME: "cloud",
                    SERVICES: [{NAME: "Kafka", ID: "1_1"}],
                },
                {
                    NAME: "edge",
                    SERVICES: [{NAME: "Producer", ID: "2_1"}],
                },
            ],
        }

        self.infra.roles = Roles(
            {
                "cloud": [
                    Host("1.1.1.1", user="root", keyfile=None, port=None),
                ],
                "monitoring": [Host("9.9.9.9")],
                "cloud.1.kafka.1": [
                    Host("1.1.1.1", user="root", keyfile=None, port=None),
                ],
                "edge.2.producer.1": [
                    Host("1.2.3.4", user="root", keyfile=None, port=None),
                ],
                "2_1": [
                    Host("1.2.3.4", user="root", keyfile=None, port=None),
                ],
            }
        )
        self.infra.config = InfrastructureConfig(config)

        user_roles = self.infra.get_filtered_user_roles()
        self.assertNotIn("monitoring", user_roles)
        self.assertIn("cloud.1.kafka.1", user_roles)
        self.assertIn("edge.2.producer.1", user_roles)

    def test_dump_manager_info(self):
        mock_1 = create_autospec(Manager)
        mock_2 = create_autospec(Manager)
        mock_3 = create_autospec(Manager)
        mock_file = Mock(spec=TextIO)

        # Mock managers
        self.infra.managers = {"test1": mock_1, "test_2": mock_2, "test_3": mock_3}

        self.infra._dump_manager_info(mock_file)

        for mock_manager in self.infra.managers.values():
            mock_manager.layers_validate_info.assert_called_once_with(mock_file)
        mock_file.write.assert_has_calls([call("# Service managers information:")])

    @patch("yaml.dump")
    @patch.object(e2cinfra.Infrastructure, "get_filtered_user_roles")
    @patch.object(InfrastructureConfig, "get_layer_names")
    def test_dump_infra_info(
        self, m_layer_names: Mock, m_user_roles: Mock, m_dump: Mock
    ):
        mock_file = Mock(spec=TextIO)
        m_layer_names.return_value = ("cloud", "fog", "edge")
        m_user_roles.return_value = Roles(
            {
                "cloud.server.1.1": [Host("1.1.1.1")],
                "cloud": [Host("1.1.1.1")],
                "fog.gateway.1.1": [Host("2.2.2.2")],
                "fog": [Host("2.2.2.2")],
                "edge.producer.1.1": [Host("3.3.3.3")],
                "edge.producer.1.2": [Host("4.4.4.4")],
                "edge": [Host("3.3.3.3")],
            }
        )

        self.infra._dump_infra_info(mock_file)

        expected = {
            "cloud": [{"cloud.server.1.1": ["1.1.1.1"]}],
            "fog": [{"fog.gateway.1.1": ["2.2.2.2"]}],
            "edge": [
                {"edge.producer.1.1": ["3.3.3.3"]},
                {"edge.producer.1.2": ["4.4.4.4"]},
            ],
        }
        m_dump.assert_called_once_with(expected, mock_file)
