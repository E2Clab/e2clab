"""
Testing CLI interface
"""

import os
import shutil
from pathlib import Path
from unittest.mock import MagicMock, Mock, call, patch

from click.testing import CliRunner

import e2clab.cli as e2cli
from e2clab import ENV_FILE
from e2clab.constants import (
    ENV_ARTIFACTS_DIR,
    ENV_SCENARIO_DIR,
    PATH_SERVICES_PLUGINS,
    Command,
    ConfFiles,
    WorkflowTasks,
)
from e2clab.services import get_available_services
from e2clab.tests.unit import TestE2cLab


class TestCLI(TestE2cLab):
    """
    Testing CLI interface
    """

    # TODO: use setUpClass for better performance
    def setUp(self):
        self.test_service = self.test_folder / "service" / "Default2.py"
        self.test_notaservice = self.test_folder / "service" / "Default3.py"

        self.test_artifacts_dir = Path(__file__).parent.resolve()

        self.runner = CliRunner()
        self.invalid_test_folder = self.test_folder / "tmp"
        os.mkdir(self.invalid_test_folder)
        for file in self.test_folder.glob("invalid_*"):
            dest_name = file.name.replace("invalid_", "")
            dest_path = self.invalid_test_folder / dest_name
            shutil.copy(file, dest_path)

    # TODO: use tearDownClass for better performance
    def tearDown(self):
        shutil.rmtree(self.invalid_test_folder)

    @patch("enoslib.check")
    def test_check_testbeds(self, p_check: MagicMock):
        result = self.runner.invoke(e2cli.check_testbeds, [])
        self.assertEqual(result.exit_code, 0)
        p_check.assert_called_once()

    def test_check_argument(self):
        folder = str(self.test_folder)
        invalid_folder = str(self.invalid_test_folder)
        result = self.runner.invoke(e2cli.check_configuration, [folder])
        self.assertEqual(result.exit_code, 0)
        result = self.runner.invoke(e2cli.check_configuration, [folder, "-c", "deploy"])
        self.assertEqual(result.exit_code, 0)
        result = self.runner.invoke(
            e2cli.check_configuration, [folder, "-c", "notacommand"]
        )
        self.assertEqual(result.exit_code, 2)
        result = self.runner.invoke(e2cli.check_configuration, [invalid_folder])
        self.assertEqual(result.exit_code, 1)
        result = self.runner.invoke(e2cli.check_configuration, ["Notafolder"])
        self.assertEqual(result.exit_code, 2)

    def test_services_list(self):
        result = self.runner.invoke(e2cli.list_command, [])
        self.assertEqual(result.exit_code, 0)
        self.assertIn("Default", result.stdout)

    def test_services_add(self):
        result = self.runner.invoke(e2cli.add, ["dontexist"])
        self.assertEqual(result.exit_code, 2)

        is_a_folder = self.test_folder
        folder_path = self._get_filepath_str(is_a_folder)
        result = self.runner.invoke(e2cli.add, [folder_path])
        self.assertEqual(result.exit_code, 1)

        not_python_file = self.test_folder / ConfFiles.WORKFLOW
        file_path = self._get_filepath_str(not_python_file)
        result = self.runner.invoke(e2cli.add, [file_path])
        self.assertEqual(result.exit_code, 1)

        dummy_service = self.test_service
        dummy_file_path = self._get_filepath_str(dummy_service)

        # Try adding a service using a copy
        result = self.runner.invoke(e2cli.add, [dummy_file_path, "--copy"])
        self.assertEqual(result.exit_code, 0)

        # Try adding an already present service
        result = self.runner.invoke(e2cli.add, [dummy_file_path])
        self.assertEqual(result.exit_code, 1)

        file_to_clean = PATH_SERVICES_PLUGINS / dummy_service.name
        file_to_clean.unlink()

        # Try adding a serice using a symlink
        result = self.runner.invoke(e2cli.add, [dummy_file_path, "--link"])
        self.assertEqual(result.exit_code, 0)

        file_to_clean = PATH_SERVICES_PLUGINS / dummy_service.name
        file_to_clean.unlink()

    def test_incorrect_service(self):
        # Try importing an invalid service
        inv_service = self._get_filepath_str(self.test_notaservice)
        result = self.runner.invoke(e2cli.add, [inv_service])
        self.assertEqual(result.exit_code, 1)
        self.assertNotIn("Default3", get_available_services())

    def test_services_remove(self):
        result = self.runner.invoke(e2cli.remove, ["Default"])
        self.assertEqual(result.exit_code, 1)

        result = self.runner.invoke(e2cli.remove, ["Notaservice"])
        self.assertEqual(result.exit_code, 1)

        # Copying a valid dummy service to be removed
        shutil.copy(self.test_service, PATH_SERVICES_PLUGINS)

        result = self.runner.invoke(e2cli.remove, [self.test_service.stem])
        self.assertEqual(result.exit_code, 0)

    def test_parse_comma_list(self):
        app_conf = "abc,xyz,123"
        parsed_list = e2cli.parse_comma_list(None, None, app_conf)

        self.assertEqual(parsed_list, ["abc", "xyz", "123"])

    @patch("e2clab.tasks.deploy")
    def test_deploy_notapath(self, _):
        result = self.runner.invoke(e2cli.deploy, ["notapath", "notapath"])
        self.assertEqual(result.exit_code, 2)

    @patch("e2clab.tasks.deploy")
    def test_deploy_valid(self, p_deploy: MagicMock):
        # Test with a valid setup
        result = self.runner.invoke(
            e2cli.deploy, [str(self.test_folder), str(self.test_artifacts_dir)]
        )
        self.assertEqual(result.exit_code, 0)
        p_deploy.assert_called_once_with(
            self.test_folder,
            self.test_artifacts_dir,
            0,
            0,
            [],
            True,
            destroy_on_finish=False,
            env=self.test_folder,
        )

    @patch("e2clab.tasks.deploy")
    def test_deploy_destroy(self, p_deploy: Mock):
        # Test with a valid setup
        result = self.runner.invoke(
            e2cli.deploy,
            [
                str(self.test_folder),
                str(self.test_artifacts_dir),
                "--destroy",
            ],
        )
        self.assertEqual(result.exit_code, 0)
        p_deploy.assert_called_once_with(
            self.test_folder,
            self.test_artifacts_dir,
            0,
            0,
            [],
            True,
            destroy_on_finish=True,
            env=self.test_folder,
        )

    @patch("e2clab.tasks.deploy")
    def test_deploy_app_conf(self, p_deploy: MagicMock):
        # Test with an app-conf argument
        result = self.runner.invoke(
            e2cli.deploy,
            [str(self.test_folder), str(self.test_folder), "--app_conf", "abc,xyz"],
        )
        self.assertEqual(result.exit_code, 0)
        p_deploy.assert_called_once_with(
            self.test_folder,
            self.test_folder,
            0,
            0,
            ["abc", "xyz"],
            True,
            destroy_on_finish=False,
            env=self.test_folder,
        )

    @patch("e2clab.tasks.deploy")
    def test_deploy_invalid(self, p_deploy: MagicMock):
        # Testing an invalid setup
        invalid_folder = str(Path(__file__).parent.resolve())
        result = self.runner.invoke(e2cli.deploy, [invalid_folder, invalid_folder])
        self.assertEqual(result.exit_code, 1)
        self.assertIn("Invalid setup, scenario not deployed.", result.stdout)
        p_deploy.assert_not_called()

    @patch("e2clab.tasks.deploy")
    def test_deploy_invalid_filename(self, p_deploy: MagicMock):
        # Passing a filename instead of a directory
        invalid_folder = str(Path(__file__).resolve())
        result = self.runner.invoke(e2cli.deploy, [invalid_folder, invalid_folder])
        # Exit code 2 because click is exiting
        self.assertEqual(result.exit_code, 2)
        p_deploy.assert_not_called()

    @patch("e2clab.tasks.deploy")
    def test_deploy_invalid_scenarios(self, m_deploy: Mock):
        # Testing with invalid scenarios
        result = self.runner.invoke(
            e2cli.deploy,
            [
                str(self.test_folder),
                str(self.test_folder),
                "--scenarios_name",
                "test,abc",
            ],
        )
        self.assertEqual(result.exit_code, 0)
        self.assertIn("Invalid setup in test, scenario not deployed", result.stdout)
        m_deploy.assert_not_called()

    @patch("e2clab.tasks.deploy")
    @patch("e2clab.utils.is_valid_setup")
    def test_deploy_valid_scenarios(self, p_is_valid: MagicMock, p_deploy: MagicMock):
        p_is_valid.return_value = True

        result = self.runner.invoke(
            e2cli.deploy,
            [
                str(self.test_folder),
                str(self.test_folder),
                "--scenarios_name",
                "test,abc",
            ],
        )
        self.assertEqual(result.exit_code, 0)
        # Second scenario call
        path2 = self.test_folder.resolve() / "abc"
        p_deploy.assert_called_with(
            path2,
            self.test_folder,
            0,
            0,
            [],
            True,
            env=path2,
        )
        p_is_valid.assert_called_with(path2, self.test_folder, Command.DEPLOY)

    @patch("e2clab.tasks.infra")
    def test_layers_services_valid(self, p_infra: MagicMock):
        # Testing a correct layers_services setup
        result = self.runner.invoke(
            e2cli.layers_services, [str(self.test_folder), str(self.test_artifacts_dir)]
        )
        self.assertEqual(result.exit_code, 0)
        p_infra.assert_called_once_with(
            self.test_folder, self.test_artifacts_dir, env=self.test_folder
        )

    @patch("e2clab.tasks.infra")
    def test_layers_services_invalid(self, p_infra: MagicMock):
        # Testing an invalid layers_services setup
        result = self.runner.invoke(
            e2cli.layers_services,
            [str(self.invalid_test_folder), str(self.invalid_test_folder)],
        )
        self.assertEqual(result.exit_code, 1)
        self.assertIn("Invalid setup,", result.stdout)

    @patch("e2clab.tasks.network")
    def test_network_valid(self, p_infra: MagicMock):
        result = self.runner.invoke(e2cli.network, [str(self.test_folder)])
        self.assertEqual(result.exit_code, 0)
        p_infra.assert_called_once_with(env=self.test_folder)

    @patch("e2clab.tasks.network")
    def test_network_invalid(self, p_infra: MagicMock):
        result = self.runner.invoke(e2cli.network, [str(self.invalid_test_folder)])
        self.assertEqual(result.exit_code, 1)
        self.assertIn("Invalid setup", result.stdout)

    @patch("e2clab.tasks.app")
    def test_workflow_valid(self, p_app: MagicMock):
        result = self.runner.invoke(e2cli.workflow, [str(self.test_folder), "prepare"])
        self.assertEqual(result.exit_code, 0)
        p_app.assert_called_once_with(task=WorkflowTasks.PREPARE, env=self.test_folder)

    @patch("e2clab.tasks.app")
    def test_workflow_invalid(self, p_app: MagicMock):
        result = self.runner.invoke(
            e2cli.workflow, [str(self.invalid_test_folder), "prepare"]
        )
        self.assertEqual(result.exit_code, 1)
        self.assertIn("Invalid setup, workflow not launched", result.stdout)

    @patch("e2clab.utils.is_valid_setup")
    @patch("e2clab.tasks.app")
    @patch("e2clab.tasks.finalize")
    def test_workflow_valid_app_conf(
        self, p_finalize: Mock, p_app: Mock, p_valid: Mock
    ):
        p_valid.return_value = True
        result = self.runner.invoke(
            e2cli.workflow, [str(self.test_folder), "launch", "--app_conf", "test,opt"]
        )
        self.assertEqual(result.exit_code, 0)
        p_valid.assert_called_with(self.test_folder, None, Command.WORKFLOW, True)
        calls = [
            call(task=WorkflowTasks.LAUNCH, app_conf="test", env=self.test_folder),
            call(task=WorkflowTasks.LAUNCH, app_conf="opt", env=self.test_folder),
        ]
        p_app.assert_has_calls(calls)
        p_finalize.assert_not_called()

    @patch("e2clab.utils.is_valid_setup")
    @patch("e2clab.tasks.app")
    @patch("e2clab.tasks.finalize")
    def test_workflow_valid_app_conf_finalize(
        self, p_finalize: Mock, p_app: Mock, p_valid: Mock
    ):
        p_valid.return_value = True
        result = self.runner.invoke(
            e2cli.workflow,
            [
                str(self.test_folder),
                "launch",
                "--app_conf",
                "test,opt",
                "--finalize",
            ],
        )
        self.assertEqual(result.exit_code, 0)
        p_valid.assert_called_with(self.test_folder, None, Command.WORKFLOW, True)
        calls = [
            call(task=WorkflowTasks.LAUNCH, app_conf="test", env=self.test_folder),
            call(task=WorkflowTasks.LAUNCH, app_conf="opt", env=self.test_folder),
        ]
        p_app.assert_has_calls(calls)
        calls = [
            call(app_conf="test", env=self.test_folder),
            call(app_conf="opt", env=self.test_folder),
        ]
        p_finalize.assert_has_calls(calls)

    @patch("e2clab.utils.is_valid_setup")
    @patch("e2clab.tasks.app")
    @patch("e2clab.tasks.finalize")
    def test_workflow_valid_app_conf_not_finalize(
        self, p_finalize: MagicMock, p_app: MagicMock, p_valid: Mock
    ):
        result = self.runner.invoke(
            e2cli.workflow,
            [
                str(self.test_folder),
                "launch",
                "--app_conf",
                "test,opt",
            ],
        )
        self.assertEqual(result.exit_code, 0)
        p_app.assert_called_with(
            task=WorkflowTasks.LAUNCH, app_conf="opt", env=self.test_folder
        )
        p_finalize.assert_not_called()

    @patch("e2clab.utils.is_valid_setup")
    @patch("e2clab.tasks.app")
    @patch("e2clab.tasks.finalize")
    def test_workflow_valid_wf_finalize(
        self, p_finalize: MagicMock, p_app: MagicMock, p_valid: Mock
    ):
        result = self.runner.invoke(
            e2cli.workflow,
            [
                str(self.test_folder),
                "launch",
                "--finalize",
            ],
        )
        self.assertEqual(result.exit_code, 0)
        p_app.assert_called_with(task=WorkflowTasks.LAUNCH, env=self.test_folder)
        p_finalize.assert_called_once()

    @patch("e2clab.utils.is_valid_setup")
    @patch("e2clab.tasks.app")
    @patch("e2clab.tasks.finalize")
    def test_workflow_valid_default_wf_finalize(
        self, p_finalize: MagicMock, p_app: MagicMock, p_valid: Mock
    ):
        result = self.runner.invoke(
            e2cli.workflow,
            [
                str(self.test_folder),
                "launch",
            ],
        )
        self.assertEqual(result.exit_code, 0)
        p_app.assert_called_with(task=WorkflowTasks.LAUNCH, env=self.test_folder)
        p_finalize.assert_not_called()

    @patch("e2clab.tasks.app")
    def test_workflow_app_conf_prepare(self, p_app: MagicMock):
        result = self.runner.invoke(
            e2cli.workflow,
            [str(self.test_folder), "prepare", "--app_conf", "test,opt"],
        )
        self.assertEqual(result.exit_code, 1)
        self.assertIn("Can't use an app", result.stdout)

    @patch("e2clab.tasks.destroy")
    def test_destroy_valid(self, p_destroy: MagicMock):
        result = self.runner.invoke(e2cli.destroy, [str(self.test_folder)])
        self.assertEqual(result.exit_code, 0)
        p_destroy.assert_called_once()

    @patch("e2clab.tasks.destroy")
    def test_destroy_invalid(self, p_destroy: MagicMock):
        result = self.runner.invoke(e2cli.destroy, ["notafolder"])
        self.assertEqual(result.exit_code, 2)
        p_destroy.assert_not_called()

    @patch("e2clab.tasks.finalize")
    def test_finalize_valid(self, p_finalize: MagicMock):
        result = self.runner.invoke(e2cli.finalize, [str(self.test_folder)])
        self.assertEqual(result.exit_code, 0)
        p_finalize.assert_called_once_with(destroy=False, env=self.test_folder)

    @patch("e2clab.tasks.finalize")
    def test_finalize_invalid(self, p_finalize: MagicMock):
        result = self.runner.invoke(e2cli.finalize, [str(self.invalid_test_folder)])
        self.assertEqual(result.exit_code, 1)
        p_finalize.assert_not_called()

    @patch("e2clab.tasks.finalize")
    def test_finalize_destroy(self, p_finalize: Mock):
        result = self.runner.invoke(
            e2cli.finalize, [str(self.test_folder), "--destroy"]
        )
        self.assertEqual(result.exit_code, 0)
        p_finalize.assert_called_once_with(destroy=True, env=self.test_folder)

    @patch("e2clab.tasks.infra")
    def test_env_variables(self, mock_infra: MagicMock):
        env = {
            ENV_SCENARIO_DIR: str(self.test_folder),
            ENV_ARTIFACTS_DIR: str(self.test_artifacts_dir),
        }
        result = self.runner.invoke(e2cli.layers_services, env=env)

        mock_infra.assert_called_once_with(
            self.test_folder, self.test_artifacts_dir, env=self.test_folder
        )
        self.assertEqual(result.exit_code, 0)

    @patch("questionary.select")
    @patch("click.edit")
    def test_edit(self, p_edit: MagicMock, p_select: MagicMock):
        p_select.return_value.ask.side_effect = ["layers_services.yaml"]

        result = self.runner.invoke(e2cli.edit, [str(self.test_folder)])

        p_edit.assert_called_once_with(
            filename=f"{self.test_folder}/layers_services.yaml", extension="yaml"
        )
        self.assertEqual(result.exit_code, 0)

    def _get_filepath_str(self, not_python_file: Path) -> str:
        file_path = str(not_python_file.resolve())
        return file_path

    @patch("e2clab.tasks.ssh")
    def test_ssh(self, p_ssh: MagicMock):
        result = self.runner.invoke(e2cli.ssh, [str(self.test_folder)])
        p_ssh.assert_called_once_with(
            env=self.test_folder, forward=False, local_port=None, remote_port=None
        )
        self.assertEqual(result.exit_code, 0)

    @patch("e2clab.tasks.ssh")
    def test_ssh_fail(self, p_ssh: MagicMock):
        result = self.runner.invoke(e2cli.ssh, ["--forward", str(self.test_folder)])
        self.assertEqual(1, result.exit_code)

    @patch.object(Path, "open")
    def test_init(self, mock_file: Mock):
        scenario_dirname = "test_scenario"
        artifacts_dirname = "test_artifacts"
        result = self.runner.invoke(
            e2cli.init,
            [
                str(self.invalid_test_folder),
                "--scenario-dirname",
                scenario_dirname,
                "--artifacts-dirname",
                artifacts_dirname,
            ],
        )
        self.assertEqual(result.exit_code, 0)

        scenario_dir = self.invalid_test_folder / scenario_dirname
        artifacts_dir = self.invalid_test_folder / artifacts_dirname

        # directories have been created if they don't exist
        self.assertTrue(scenario_dir.exists())
        self.assertTrue(artifacts_dir.exists())

        mock_file.assert_called_once_with("w")

    @patch.object(Path, "open")
    def test_init_prompt(self, mock_file: Mock):
        scenario_dirname = "test_scenario"
        artifacts_dirname = "test_artifacts"

        env_file = self.invalid_test_folder / ENV_FILE
        env_file.touch()

        result = self.runner.invoke(
            e2cli.init,
            [
                str(self.invalid_test_folder),
                "--scenario-dirname",
                scenario_dirname,
                "--artifacts-dirname",
                artifacts_dirname,
            ],
            input="n",
        )
        self.assertEqual(result.exit_code, 0)
        mock_file.assert_not_called()

        result = self.runner.invoke(
            e2cli.init,
            [
                str(self.invalid_test_folder),
                "--scenario-dirname",
                scenario_dirname,
                "--artifacts-dirname",
                artifacts_dirname,
            ],
            input="y",
        )
        self.assertEqual(result.exit_code, 0)
        mock_file.assert_called_with("w")

    @patch("e2clab.tasks.get_output_dir")
    def test_get_output_dir(self, m_get_output_dir: Mock):
        result = self.runner.invoke(e2cli.get_output_dir, [str(self.test_folder)])
        self.assertEqual(result.exit_code, 0)
        m_get_output_dir.assert_called_once_with(env=self.test_folder)

    def test_ascii_help(self):
        result = self.runner.invoke(e2cli.cli, ["--help"])
        self.assertIn(e2cli.ASCII_ART, result.stdout)
        self.assertEqual(result.exit_code, 0)
