"""
Testing the e2clab.tasks module
"""

import shutil
from unittest.mock import Mock, call, patch

from enoslib import enostask

import e2clab.tasks as e2c_tasks
from e2clab.experiment import Experiment
from e2clab.tasks import e2ctask

from . import TestE2cLab


class PickableMock(Mock):
    def __reduce__(self):
        return (Mock, ())


class TestTasks(TestE2cLab):
    """
    Testing different experiment calls
    """

    # Setting up a dir to store the created envs
    @classmethod
    def setUpClass(cls) -> None:
        # Create dir to create an env object
        cls.env = cls.test_folder / "test_env_path"
        cls.env_object = cls.env / "env"
        cls.env.mkdir(exist_ok=True)

    @classmethod
    def tearDownClass(cls) -> None:
        # Remove dir
        shutil.rmtree(cls.env)


class TestDecorator(TestTasks):
    """
    We want to test the e2clab.tasks.e2ctask decorator
    """

    def test_wrong_load(self):
        """
        Try to load a wrong Experiment object
        """

        @enostask(new=True, symlink=False)
        def wrong_store(env):
            # Not storing an Experiment object
            env[e2c_tasks.EXPERIMENT] = 123

        wrong_store(env=self.env)

        @e2c_tasks.e2ctask()
        def dummy_task(env):
            pass

        with self.assertRaises(e2c_tasks.TaskError):
            dummy_task(env=self.env)

    def test_wrong_store(self):
        @e2ctask(new=True)
        def wrong_store(env):
            # Not returning an Experiment instance
            return 123

        with self.assertRaises(e2c_tasks.TaskError):
            wrong_store(env=self.env)

    def test_correct_store(self):
        @e2ctask(new=True)
        def correct_task():
            exp = Experiment(scenario_dir=self.env, artifacts_dir=self.env)
            return exp

        correct_task(env=self.env)

        self.assertTrue(self.env_object.exists())


class TestReloadingTasks(TestTasks):
    """
    Testing tasks that need to reload a previously created experiment object
    """

    def setUp(self):
        """
        Create a env containing an experiment for re-loading purposes
        """

        @enostask(new=True, symlink=False)
        def mock_infra(env):
            exp = Experiment(
                scenario_dir=self.env,
                artifacts_dir=self.env,
            )
            exp.initiate()
            env[e2c_tasks.EXPERIMENT] = exp

        mock_infra(env=self.env)

    @patch.object(Experiment, "network")
    def test_network(self, mock_network: Mock):
        e2c_tasks.network(env=self.env)
        mock_network.assert_called_once_with()
        self.assertTrue(self.env_object.exists())

    @patch.object(Experiment, "application")
    def test_app(self, mock_appliction: Mock):
        e2c_tasks.app(env=self.env, task="prepare", app_conf="base")
        mock_appliction.assert_called_once_with(task="prepare", app_conf="base")
        self.assertTrue(self.env_object.exists())

    @patch.object(Experiment, "finalize")
    def test_finalize(self, mock_finalize: Mock):
        e2c_tasks.finalize(env=self.env)
        mock_finalize.assert_called_once_with(app_conf=None, destroy=False)
        self.assertTrue(self.env_object.exists())

    @patch.object(Experiment, "ssh")
    def test_ssh(self, mock_ssh: Mock):
        e2c_tasks.ssh(env=self.env, forward=True, local_port=8080, remote_port=8080)
        mock_ssh.assert_called_once_with(
            forward=True, local_port=8080, remote_port=8080
        )
        self.assertTrue(self.env_object.exists())

    @patch.object(Experiment, "get_output_dir")
    def test_get_output_dir(self, mock_get_output_dir: Mock):
        e2c_tasks.get_output_dir(env=self.env)
        mock_get_output_dir.assert_called_once_with()
        self.assertTrue(self.env_object.exists())


class TestCreatingTasks(TestTasks):
    """
    Testing Tasks that create an experiment and store them in an env
    """

    @patch.object(Experiment, "infrastructure")
    @patch.object(Experiment, "initiate")
    def test_infra(self, mock_initiate: Mock, mock_infrastructure: Mock):
        e2c_tasks.infra(scenario_dir=self.env, artifacts_dir=self.env, env=self.env)
        mock_infrastructure.assert_called_once_with()
        mock_initiate.assert_called_once_with()
        self.assertTrue(self.env_object.exists())

    @patch.object(Experiment, "deploy")
    def test_deploy(self, mock_deploy: Mock):
        e2c_tasks.deploy(
            env=self.env,
            scenario_dir=self.env,
            artifacts_dir=self.env,
            duration=60,
            repeat=3,
        )
        # Ran once, repeated 3 times
        mock_deploy.assert_has_calls(
            [
                call(duration=60, is_prepare=True, destroy_on_finish=False),
                call(duration=60, is_prepare=True, destroy_on_finish=False),
                call(duration=60, is_prepare=True, destroy_on_finish=False),
                call(duration=60, is_prepare=True, destroy_on_finish=False),
            ]
        )
        self.assertTrue(self.env_object.exists())
