"""
Testing Provenance manager
"""

from unittest.mock import Mock, patch

from enoslib import Host

from e2clab.services.provenance.provenance import Provenance

from . import TestE2cLab


class TestProvenance(TestE2cLab):
    """
    Testing Provenance
    """

    PARALLELISM = 3

    def setUp(self):
        self.prov = Provenance(
            host=Host("1.1.1.1"),
            agent=[Host("2.2.2."), Host("3.3.3.3")],
            parallelism=self.PARALLELISM,
            dataflow_spec="path",
        )

    def test_init(self):
        self.assertIsInstance(self.prov.host, Host)
        self.assertIsInstance(self.prov.agent[0], Host)
        self.assertEqual(len(self.prov.agent), 2)
        self.assertEqual(self.prov.dataflow_spec, "path")

    @patch("enoslib.actions")
    def test_backup(self, p_actions: Mock):
        self.prov.backup("./data/")
        p_actions.assert_called_once_with(roles=Host("1.1.1.1"))
        # context manager
        mock_a = p_actions.return_value.__enter__.return_value
        mock_a.shell.assert_called_once()
        self.assertEqual(mock_a.fetch.call_count, self.PARALLELISM + 1)

    @patch("enoslib.actions")
    def test_destroy(self, p_actions: Mock):
        self.prov.destroy()
        p_actions.assert_called_once_with(roles=Host("1.1.1.1"))
        # context manager
        mock_a = p_actions.return_value.__enter__.return_value
        self.assertEqual(mock_a.shell.call_count, 3)

    # TODO: improve testing
    @patch("enoslib.run")
    @patch("enoslib.Docker")
    @patch("enoslib.actions")
    def test_deploy(self, p_actions: Mock, p_docker: Mock, p_run: Mock):
        self.prov.deploy()
        p_docker.assert_called_once()
