"""
Testing the managers package
"""

from datetime import datetime
from ipaddress import IPv4Network, IPv6Network
from pathlib import Path
from unittest.mock import Mock, patch

from enoslib import (
    DefaultNetwork,
    Dstat,
    Host,
    Networks,
    Roles,
    TIGMonitoring,
    TPGMonitoring,
)

import e2clab.constants.default as default
from e2clab.constants import Environment
from e2clab.constants.layers_services import (
    ARCHI,
    CLUSTER,
    DSTAT_OPTIONS,
    MONITORING_IOT_CURRENT,
    MONITORING_IOT_POWER,
    MONITORING_IOT_PROFILES,
    MONITORING_IOT_VOLTAGE,
    MONITORING_SERVICE_ROLE,
    MONITORING_SVC_AGENT_CONF,
    MONITORING_SVC_DSTAT,
    MONITORING_SVC_PROVIDER,
    MONITORING_SVC_TIG,
    MONITORING_SVC_TPG,
    MONITORING_SVC_TYPE,
    NAME,
    PROVENANCE_SERVICE_ROLE,
    PROVENANCE_SVC_DATAFLOW_SPEC,
    PROVENANCE_SVC_PARALLELISM,
    PROVENANCE_SVC_PROVIDER,
)
from e2clab.errors import E2clabConfigError, E2clabError
from e2clab.managers.manager import Manager
from e2clab.managers.monitoring import MonitoringManager, MonitoringType
from e2clab.managers.monitoring_iot import MonitoringIoTManager
from e2clab.managers.monitoring_kwollect import (
    END,
    METRICS,
    START,
    STEP,
    MonitoringKwollectManager,
)
from e2clab.managers.provenance import ProvenanceManager
from e2clab.probe import Record, TaskProbe
from e2clab.providers.plugins.Iotlab import Iotlab
from e2clab.services import Provenance
from e2clab.tests.unit import TestE2cLab


class TestAbstract(TestE2cLab):
    def test_instanciation(self):
        class Test(Manager):
            pass

        with self.assertRaises(TypeError):
            Test()


class TestProvenanceManager(TestE2cLab):
    default_config = {
        PROVENANCE_SVC_PROVIDER: Environment.G5K.value,
        CLUSTER: "parasilo",
        PROVENANCE_SVC_DATAFLOW_SPEC: "dataflow_spec_file",
    }

    config = {
        PROVENANCE_SVC_PROVIDER: Environment.G5K.value,
        CLUSTER: "parasilo",
        PROVENANCE_SVC_DATAFLOW_SPEC: "dataflow_spec_file",
        PROVENANCE_SVC_PARALLELISM: 3,
    }

    def setUp(self):
        self.host = Host("1.1.1.1")
        self.agents = [Host("2.2.2.2"), Host("3.3.3.3")]
        self.artifacts = Path(__name__)
        roles = Roles(
            {
                ProvenanceManager.SERVICE_ROLE: [self.host],
                ProvenanceManager.ROLE: self.agents,
            }
        )
        self.prov = ProvenanceManager(config=self.config)
        self.prov.init(roles=roles, artifacts_dir=self.artifacts)

    def test_init_method(self):
        self.assertEqual(self.prov.host, self.host)
        self.assertCountEqual(self.prov.agent, self.agents)

    def test_config_validation(self):
        with self.assertRaises(E2clabConfigError):
            ProvenanceManager(config={})

    def test_init(self):
        self.assertIsInstance(self.prov, ProvenanceManager)
        self.assertIsInstance(self.prov.service, Provenance)

    @patch.object(ProvenanceManager, "_get_parallelism")
    @patch.object(ProvenanceManager, "_get_dataflow_spec")
    def test_create_service(self, m_dataflow_spec: Mock, m_parallelism: Mock):
        m_dataflow_spec.return_value = "test"
        m_parallelism.return_value = 2

        provenance_service = self.prov.create_service()
        self.assertIsInstance(provenance_service, Provenance)
        self.assertEqual(provenance_service.host, Host("1.1.1.1"))
        self.assertCountEqual(
            provenance_service.agent, [Host("2.2.2.2"), Host("3.3.3.3")]
        )
        self.assertEqual(provenance_service.dataflow_spec, str(self.artifacts / "test"))
        self.assertEqual(provenance_service.parallelism, 2)

        m_dataflow_spec.assert_called_once()
        m_parallelism.assert_called_once()

    def test_get_parallelism(self):
        self.assertEqual(self.prov._get_parallelism(), 3)
        self.assertIsInstance(self.prov._get_parallelism(), int)

        prov = ProvenanceManager(config=self.default_config)
        self.assertEqual(prov._get_parallelism(), 1)

    def test_get_dataflow_spec(self):
        self.assertEqual(self.prov._get_dataflow_spec(), "dataflow_spec_file")
        self.assertIsInstance(self.prov._get_dataflow_spec(), str)

    @patch.object(Provenance, "deploy")
    def test_deploy(self, m_deploy: Mock):
        self.prov._deploy()
        m_deploy.assert_called_once_with()

    @patch.object(Provenance, "backup")
    def test_backup(self, m_backup: Mock):
        self.prov._backup(output_dir=Path("here"))
        # m_backup.assert_called_once_with("here")
        m_backup.assert_called_once()

    def test_get_extra_info(self):
        extra_info = self.prov.get_extra_info()
        self.assertIn(PROVENANCE_SERVICE_ROLE, extra_info)
        self.assertEqual(extra_info[PROVENANCE_SERVICE_ROLE]["__address__"], "1.1.1.1")

    @patch.object(Provenance, "destroy")
    def test_destroy(self, m_destroy: Mock):
        self.prov._destroy()
        m_destroy.assert_called_once_with()

    def test_get_environment(self):
        self.assertEqual(Environment.G5K, self.prov.get_environment())

    def test_deploy_provenance(self):
        m_service = Mock()
        self.prov.service = m_service
        self.prov._deploy()
        m_service.deploy.assert_called_once()

    def test_backup_provenance(self):
        m_service = Mock()
        self.prov.service = m_service
        backup_dir = Path(__name__) / default.PROVENANCE_DATA
        self.prov._backup(Path(__name__))
        m_service.backup.assert_called_once_with(backup_dir=backup_dir)

    def test_destroy_provenance(self):
        m_service = Mock()
        self.prov.service = m_service
        self.prov._destroy()
        m_service.destroy.assert_called_once()


class TestMonitoringManager(TestE2cLab):
    config_dstat = {
        MONITORING_SVC_TYPE: MONITORING_SVC_DSTAT,
        DSTAT_OPTIONS: "-m -c",
    }
    config_tig = {
        MONITORING_SVC_TYPE: MONITORING_SVC_TIG,
        MONITORING_SVC_PROVIDER: Environment.G5K.value,
        MONITORING_SVC_AGENT_CONF: "test",
    }
    config_tpg = {
        MONITORING_SVC_TYPE: MONITORING_SVC_TPG,
        MONITORING_SVC_PROVIDER: Environment.G5K.value,
        MONITORING_SVC_AGENT_CONF: "test",
    }

    def setUp(self):
        self.host_address = "1.1.1.1"
        self.host = Host(self.host_address)
        self.agents = [Host("2.2.2.2"), Host("3.3.3.3")]
        self.network = DefaultNetwork("1.2.3.0/24")
        self.networks = Networks(all=[self.network])
        self.artifacts_dir = self.test_folder
        self.roles = Roles(
            {
                MonitoringManager.SERVICE_ROLE: [self.host],
                MonitoringManager.ROLE: self.agents,
            }
        )
        self.meta = {"remote_working_dir": "/test"}

        self.monit_dstat = MonitoringManager(config=self.config_dstat)
        self.monit_dstat.init(
            self.roles,
            networks=self.networks,
            artifacts_dir=self.artifacts_dir,
            meta=self.meta,
        )

        self.monit_tig = MonitoringManager(config=self.config_tig)
        self.monit_tig.init(
            self.roles,
            networks=self.networks,
            artifacts_dir=self.artifacts_dir,
            meta=self.meta,
        )

        self.monit_tpg = MonitoringManager(config=self.config_tpg)
        self.monit_tpg.init(
            self.roles,
            networks=self.networks,
            artifacts_dir=self.artifacts_dir,
            meta=self.meta,
        )

    def test_init_method(self):
        self.assertEqual(self.monit_dstat.host, self.host)
        self.assertCountEqual(self.monit_dstat.agent, self.agents)

    def test_get_monitoring_type(self):
        monitoring_type = self.monit_dstat._get_monitoring_type()
        self.assertEqual(monitoring_type, MonitoringType.DSTAT)

    @patch.object(MonitoringManager, "_get_nets")
    def test_service_instance(self, m_get_nets: Mock):
        self.assertIsInstance(self.monit_dstat.service, Dstat)
        self.assertIsInstance(self.monit_tig.service, TIGMonitoring)
        self.assertIsInstance(self.monit_tpg.service, TPGMonitoring)

    def test_get_environment(self):
        env = self.monit_dstat.get_environment()
        self.assertIsNone(env)

        env = self.monit_tig.get_environment()
        self.assertEqual(env, Environment.G5K)

    def test_deploy(self):
        m_service = Mock()
        self.monit_dstat.service = m_service
        self.monit_dstat._deploy()
        m_service.deploy.assert_called_once()

    def test_backup(self):
        m_service = Mock()
        self.monit_dstat.service = m_service
        backup_dir = Path(__name__) / default.MONITORING_DATA
        self.monit_dstat._backup(Path(__name__))
        m_service.backup.assert_called_once_with(backup_dir=backup_dir)

    def test_destroy(self):
        m_service = Mock()
        self.monit_dstat.service = m_service
        self.monit_dstat._destroy()
        m_service.destroy.assert_called_once()

    def test_get_nets(self):
        self.monit_dstat.networks = Networks(
            {
                "Test1_ipv4": [DefaultNetwork("10.0.0.1/24")],
                "Test2_ipv4": [DefaultNetwork("10.0.0.2/24")],
                "Test3_ipv4": [DefaultNetwork("10.0.0.3/24")],
                "Test3_ipv6": [DefaultNetwork("2001:db8:1234::/48")],
            }
        )

        nets = self.monit_dstat._get_nets(self.monit_dstat.networks, IPv4Network)
        self.assertEqual(len(nets), 3)

        nets = self.monit_dstat._get_nets(self.monit_dstat.networks, IPv6Network)
        self.assertEqual(len(nets), 1)

    def test_get_monitoring_agent_conf(self):
        self.assertIsNone(self.monit_dstat._get_monitoring_agent_conf())
        self.assertEqual(self.monit_tig._get_monitoring_agent_conf(), "test")
        self.assertEqual(self.monit_tpg._get_monitoring_agent_conf(), "test")

    @patch.object(MonitoringManager, "_get_monitoring_agent_conf")
    def test_get_agent_conf(self, m_get_magent_conf: Mock):
        m_get_magent_conf.return_value = None
        self.assertIsNone(self.monit_dstat._get_agent_conf())
        m_get_magent_conf.assert_called_once_with()

        # Just taking a random existing file
        m_get_magent_conf.return_value = "test"
        self.monit_tig.artifacts_dir = self.test_folder
        expected_result = self.test_folder / "test"

        agent_conf = self.monit_tig._get_agent_conf()
        self.assertEqual(agent_conf, expected_result)

    def test_get_dstat_options(self):
        self.assertEqual(self.monit_dstat._get_dstat_options(), "-m -c")
        self.assertEqual(self.monit_tig._get_dstat_options(), default.DSTAT_OPTS)

    @patch.object(MonitoringManager, "_get_monitoring_type")
    def test_get_extra_info(self, m_monitoring_type: Mock):
        m_monitoring_type.return_value = MonitoringType.DSTAT
        self.assertEqual(self.monit_dstat.get_extra_info(), {})

        expected_output = {
            MONITORING_SERVICE_ROLE: {
                "__address__": f"{self.host_address}",
                "url": f"http://{self.host_address}:3000",
            }
        }

        m_monitoring_type.return_value = MonitoringType.TIG
        self.assertEqual(self.monit_tig.get_extra_info(), expected_output)

        m_monitoring_type.return_value = MonitoringType.TPG
        self.assertEqual(self.monit_tpg.get_extra_info(), expected_output)


class TestMonitoringIOTManager(TestE2cLab):
    def setUp(self):
        config = {
            MONITORING_IOT_PROFILES: [
                {
                    NAME: "test",
                    ARCHI: "a8",
                    MONITORING_IOT_CURRENT: True,
                    MONITORING_IOT_POWER: True,
                    MONITORING_IOT_VOLTAGE: True,
                }
            ]
        }
        self.monit_iot = MonitoringIoTManager(config=config)
        self.monit_iot.init(Roles(), Networks(), None, Mock(spec=Iotlab), {})

    def test_init(self):
        self.assertIsNone(self.monit_iot.host)
        self.assertEqual(len(self.monit_iot.agent), 0)

    def test_backup(self):
        m_enoslib_provider = Mock()
        self.monit_iot.provider.provider = m_enoslib_provider
        self.monit_iot._backup(output_dir=self.test_folder)
        expected_output_dir = (
            self.test_folder / default.MONITORING_DATA / default.MONITORING_IOT_DATA
        )
        m_enoslib_provider.collect_data_experiment.assert_called_once_with(
            expected_output_dir
        )

    def test_get_environment(self):
        self.assertEqual(self.monit_iot.get_environment(), Environment.IOT_LAB)


class TestKwollectManager(TestE2cLab):
    def setUp(self):
        config = {
            METRICS: [
                "wattmetre_power_watt",
                "prom_default_metrics",
                "pdu_outlet_power_watt",
            ],
            STEP: "launch",
        }
        self.kwo_manager = MonitoringKwollectManager(config)

    def test_schema(self):
        badconf = {METRICS: ["all"], STEP: "notastep"}
        with self.assertRaises(E2clabConfigError):
            MonitoringKwollectManager(badconf)

        badconf = {STEP: "launch"}
        with self.assertRaises(E2clabConfigError):
            MonitoringKwollectManager(badconf)

        badconf = {METRICS: ["all"], END: "launch"}
        with self.assertRaises(E2clabConfigError):
            MonitoringKwollectManager(badconf)

        badconf = {METRICS: ["all"], STEP: "launch", START: "launch"}
        with self.assertRaises(E2clabConfigError):
            MonitoringKwollectManager(badconf)

        validconf = {METRICS: ["all"], STEP: "launch"}
        MonitoringKwollectManager(validconf)

        validconf = {METRICS: ["all"], START: "launch", END: "finalize"}
        MonitoringKwollectManager(validconf)

        validconf = {METRICS: ["all"], START: "launch"}
        MonitoringKwollectManager(validconf)

    def test_get_environment(self):
        self.assertEqual(self.kwo_manager.get_environment(), Environment.G5K)

    def test_get_metrics_str(self):
        metrics_str = self.kwo_manager._get_metrics_str()
        excepted = "wattmetre_power_watt,prom_default_metrics,pdu_outlet_power_watt"
        self.assertEqual(metrics_str, excepted)

        test_config = {METRICS: ["test", "testtest", "all"]}
        manager = MonitoringKwollectManager(test_config)
        self.assertEqual("all", manager._get_metrics_str())

    def test_filter_site_nodes(self):
        self.kwo_manager.agent = [
            Host("paradoxe-1.rennes.grid5000.fr"),
            Host("paradoxe-2.rennes.grid5000.fr"),
            Host("ecotype-1.nantes.grid5000.fr"),
            Host("anothernode.test.com"),
        ]
        self.assertEqual(
            "paradoxe-1,paradoxe-2", self.kwo_manager._filter_site_nodes("rennes")
        )
        self.assertEqual("ecotype-1", self.kwo_manager._filter_site_nodes("nantes"))
        self.assertIsNone(self.kwo_manager._filter_site_nodes("nancy"))

    @patch.object(TaskProbe, "get_task_record")
    def test_get_timestamp_start_none(self, m_get_rec: Mock):
        m_get_rec.return_value = Record(start=None, end=None)
        with self.assertRaises(E2clabError):
            self.kwo_manager._get_timestamp_str()

    @patch.object(TaskProbe, "get_task_record")
    def test_get_timestamp_record_is_none(self, m_get_rec: Mock):
        m_get_rec.return_value = None
        with self.assertRaises(E2clabError):
            self.kwo_manager._get_timestamp_str()

    @patch.object(TaskProbe, "get_task_record")
    def test_get_timestamp_str_ok(self, m_get_rec: Mock):
        m_get_rec.return_value = Record(start=datetime.now(), end=None)
        start, end = self.kwo_manager._get_timestamp_str()
        self.assertIsInstance(start, str)
        self.assertIsNone(end)

    @patch.object(TaskProbe, "get_task_record")
    def test_get_timestamp_default(self, m_get_rec: Mock):
        m_get_rec.return_value = Record(start=datetime.now(), end=None)
        config = {
            METRICS: [
                "wattmetre_power_watt",
                "prom_default_metrics",
                "pdu_outlet_power_watt",
            ],
            # STEP: "launch",
        }
        kwo_manager = MonitoringKwollectManager(config)
        kwo_manager._get_timestamp_str()
        # Assert calls the default
        m_get_rec.assert_called_once_with(default.KWOLLECT_STEP)
