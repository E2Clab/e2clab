"""
Testing app.py module
"""

from pathlib import Path
from unittest.mock import Mock, patch

from enoslib import Host, Roles
from enoslib.infra.enos_chameleonedge.objects import ChameleonDevice

from e2clab.app import App
from e2clab.config import WorkflowEnvConfig
from e2clab.constants import ConfFiles

from . import TestE2cLab


class TestApp(TestE2cLab):
    """Testing app.App class"""

    def setUp(self):
        self.app = App(
            config=self.test_folder / ConfFiles.WORKFLOW,
            experiment_dir=Path("."),
            scenario_dir=self.test_folder,
            artifacts_dir=self.test_folder,
            roles=Roles,
            all_serv_extra_inf={},
        )
        self.app_app_conf = App(
            config=self.test_folder / ConfFiles.WORKFLOW,
            experiment_dir=Path("."),
            scenario_dir=self.test_folder,
            artifacts_dir=self.test_folder,
            roles=Roles,
            all_serv_extra_inf={},
            app_conf="base",
            env_config=self.test_folder / ConfFiles.WORKFLOW_ENV,
            optimization_config="test_optim",
        )

    def test_init(self):
        self.assertEqual(self.app.app_dir, self.app.experiment_dir)
        self.assertEqual(self.app_app_conf.app_dir, self.app.experiment_dir / "base")

    def test_load_env_config(self):
        wenv_conf = self.app._load_env_config(Path("Notapath"))
        self.assertIsInstance(wenv_conf, WorkflowEnvConfig)
        self.assertCountEqual(wenv_conf.values(), [])

        wenv_conf = self.app._load_env_config(self.test_folder / ConfFiles.WORKFLOW_ENV)
        self.assertIsInstance(wenv_conf, WorkflowEnvConfig)
        self.assertNotEqual(len(wenv_conf.values()), 0)

    def test_vars_to_inject_ansible(self):
        # Like a 'prepare' task
        self.app_app_conf.working_dir = self.app_app_conf.artifacts_dir
        self.app_app_conf.current_repeat = 1
        extra_vars = self.app_app_conf._vars_to_inject_ansible()

        self.assertIsInstance(extra_vars, dict)
        self.assertEqual(extra_vars["working_dir"], str(self.test_folder))
        self.assertEqual(extra_vars["artifacts_dir"], str(self.test_folder))
        self.assertEqual(extra_vars["scenario_dir"], str(self.test_folder))
        self.assertEqual(extra_vars["optimization_config"], "test_optim")
        self.assertEqual(extra_vars["current_repeat"], 1)
        self.assertEqual(extra_vars["app_conf"], "base")
        self.assertEqual(extra_vars["env_time"], 30)

    def test_add_gateway_in_prefix(self):
        hosts = [
            Host("1.1.1.1", extra={"gateway": "1.2.3.4"}),
            Host("2.2.2.2", extra={"gateway": "1.2.3.4"}),
        ]
        prefix = "__test__"
        new_hosts = self.app._add_gateway_in_prefix(hosts, prefix)
        self.assertEqual(len(new_hosts), 2)
        for h in new_hosts:
            self.assertIn("gateway", h.extra["__test__"])
            self.assertEqual("1.2.3.4", h.extra["__test__"]["gateway"])

    def test_inject_vars(self):
        command = "cp {{ source }} {{ dest }}"
        extra_vars = {"source": "/tmp/script.sh"}
        device_extra = {"dest": "/usr/root/"}
        rendered_cmd = self.app._inject_vars(command, extra_vars, device_extra)
        self.assertEqual(rendered_cmd, "cp /tmp/script.sh /usr/root/")

    @patch.object(App, "_inject_vars")
    def test_build_dev_command_from_ansible(self, m_inject: Mock):
        command = "cp {{ source }} {{ dest }}"
        extra_vars = {"source": "/tmp/script.sh"}
        device_extra = {"dest": "/usr/root/"}
        rendered_cmd = self.app._build_dev_command_from_ansible(
            command, extra_vars, device_extra, []
        )
        m_inject.assert_called_once()
        self.assertEqual(rendered_cmd, m_inject.return_value)

    def test_add_extra_chameleondevice(self):
        devices = [
            ChameleonDevice("1.1.1.1", [], "1", "test.rc"),
            ChameleonDevice("2.2.2.2", [], "2", "test.rc"),
        ]
        new_devices = self.app._add_extra_chameleondevice(devices)
        for device in new_devices:
            self.assertTrue(hasattr(device, "extra"))

    def test_device_self_extra_info(self):
        devices = [
            ChameleonDevice("1.1.1.1", [], "1", "test.rc"),
            ChameleonDevice("2.2.2.2", [], "2", "test.rc"),
            ChameleonDevice("3.3.3.3", [], "3", "test.rc"),
        ]
        for device in devices:
            setattr(device, "extra", {})
        self.app.all_serv_extra_inf = {
            "worker": {"__address__": "1.1.1.1", "_id": "1_1"},
            "gate": {"__address__": "2.2.2.2", "_id": "1_2"},
            "sensor": {"__address__": "3.3.3.3", "_id": "1_3"},
        }
        new_devices = self.app._device_self_extra_info(devices)
        self.assertEqual(len(new_devices), 3)
        self.assertEqual(new_devices[0].extra["_self"]["_id"], "1_1")
        self.assertEqual(new_devices[0].extra["_self"]["__address__"], "1.1.1.1")

    def test_merge_depends_on(self):
        pass
