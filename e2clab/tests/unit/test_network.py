"""
Testing network.py module
"""

from pathlib import Path
from unittest.mock import Mock, patch

import enoslib as en
from enoslib import Host, Roles
from enoslib.infra.enos_chameleonedge.objects import ChameleonDevice

import e2clab.constants.default as default
from e2clab.config import NetworkConfig
from e2clab.constants import ConfFiles
from e2clab.constants.layers_services import MONITORING_NETWORK_ROLE
from e2clab.constants.network import DELAY, DST, LOSS, RATE, SRC, SYMMETRIC
from e2clab.errors import E2clabConfigError, E2clabFileError
from e2clab.network import Network
from e2clab.tests.unit import TestE2cLab


class TestNetwork(TestE2cLab):
    """
    Testing Network orchestration class
    """

    def setUp(self) -> None:
        correct_conf = self.test_folder / ConfFiles.NETWORK

        self.net = Network(config=correct_conf, roles=None, networks=None)

    def test_init(self):
        self.assertIsNone(self.net.roles)
        self.assertIsNone(self.net.networks)
        self.assertIsInstance(self.net.config, NetworkConfig)

    @patch.object(Network, "_get_filtered_networks")
    def test_prepare(self, m_get_filtered: Mock):
        m_get_filtered.return_value = [en.DefaultNetwork("10.0.0.1/24")]
        self.net.prepare()

    @patch.object(Network, "_configure_netemhtb")
    @patch.object(Network, "_configure_netem")
    @patch.object(Network, "_check_edgeChameleonDevice")
    @patch.object(NetworkConfig, "get_networks")
    def test_deploy(
        self, m_get_networks: Mock, m_cham_dev: Mock, m_netem: Mock, m_netemhtb: Mock
    ):
        # Case no network emulation
        m_get_networks.return_value = None
        self.assertIsNone(self.net.deploy())

        # Case network emulations
        edge_net_conf = {SRC: "cloud", DST: "edge", DELAY: "100ms"}
        fog_net_conf = {SRC: "cloud", DST: "fog", DELAY: "50ms"}
        m_get_networks.return_value = [
            edge_net_conf,
            fog_net_conf,
        ]
        m_cham_dev.side_effect = [True, False]

        net_to_em = [en.DefaultNetwork("10.0.0.1/24")]
        self.net.networks_to_em = net_to_em

        self.net.deploy()

        m_netem.assert_called_once_with(edge_net_conf)
        m_netemhtb.assert_called_once_with(fog_net_conf, net_to_em)
        m_netem.return_value.deploy.assert_called_once_with()
        m_netemhtb.return_value.deploy.assert_called_once_with()

        self.assertIn("cloud-edge-asymmetric", self.net.netems)
        self.assertEqual(self.net.netems["cloud-edge-asymmetric"], m_netem.return_value)
        self.assertIn("cloud-fog-asymmetric", self.net.netems)
        self.assertEqual(
            self.net.netems["cloud-fog-asymmetric"], m_netemhtb.return_value
        )

    @patch.object(Path, "mkdir")
    def test_validate(self, m_path_mkdir: Mock):
        m_netem = Mock(spec=en.Netem)
        self.net.netems = {"cloud-edge-symmetric": m_netem}
        self.net.validate(Path("/tmp"))

        m_path_mkdir.assert_called_once()
        m_netem.validate.assert_called_once_with(
            output_dir=Path(f"/tmp/{default.NET_VALIDATE_DIR}/cloud-edge-symmetric/")
        )

    def test_destroy(self):
        with self.assertRaises(NotImplementedError):
            self.net.destroy()

    def test_load_invalid_config(self):
        with self.assertRaises(E2clabConfigError):
            Network(
                config=self.test_folder / "invalid_network.yaml",
                roles=None,
                networks=None,
            )

        with self.assertRaises(E2clabFileError):
            Network(config="notafile", roles=None, networks=None)

    def test_configure_netemhtb(self):
        self.net.roles = Roles({"cloud": [Host("1.1.1.1")], "edge": [Host("2.2.2.2")]})

        net_config = {
            SRC: "cloud",
            DST: "edge",
            SYMMETRIC: True,
            DELAY: "100ms",
            RATE: "1gbit",
            LOSS: "1%",
        }
        networks_to_em = [en.DefaultNetwork("10.0.0.1/24")]

        netem = self.net._configure_netemhtb(net_config, networks_to_em)
        self.assertIsInstance(netem, en.NetemHTB)
        self.assertCountEqual(netem.sources, [Host("1.1.1.1"), Host("2.2.2.2")])

    def test_configure_netem(self):
        self.net.roles = Roles({"cloud": [Host("1.1.1.1")], "edge": [Host("2.2.2.2")]})

        net_config = {
            SRC: "cloud",
            DST: "edge",
            SYMMETRIC: True,
            DELAY: "100ms",
            RATE: "1gbit",
            LOSS: "1%",
        }

        # Cant do much more testing on the constraints for now with enoslib API
        netem = self.net._configure_netem(net_config)
        self.assertIsInstance(netem, en.Netem)
        self.assertCountEqual(netem.sources, [Host("1.1.1.1")])

    def test_check_edgeChameleonDevice(self):
        self.net.roles = Roles(
            {
                "cloud": [Host("1.1.1.1")],
                "fog": [Host("2.2.2.2")],
                "edge": [ChameleonDevice("2.2.2.2", ["edge"], "abc", "test.rc")],
            }
        )

        net_config_true = {
            SRC: "cloud",
            DST: "edge",
        }
        net_config_false = {
            SRC: "cloud",
            DST: "fog",
        }
        self.assertTrue(self.net._check_edgeChameleonDevice(net_config_true))
        self.assertFalse(self.net._check_edgeChameleonDevice(net_config_false))

    def test_get_filtered_networks(self):
        monit_net = en.DefaultNetwork("10.0.0.1/24")
        monit_test = en.DefaultNetwork("20.0.0.1/24")
        self.net.networks = en.Networks(
            {
                MONITORING_NETWORK_ROLE: [monit_net],
                "TEST_NETWORK": [monit_test],
            }
        )
        networks_to_em = self.net._get_filtered_networks()
        self.assertCountEqual(networks_to_em, [monit_test])
        self.assertEqual(id(networks_to_em[0]), id(monit_test))
