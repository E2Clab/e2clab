"""
Testing the probe.py module
"""

import shutil

from e2clab.constants import WorkflowTasks
from e2clab.probe import TaskProbe

from . import TestE2cLab


class TestTaskProbe(TestE2cLab):
    @classmethod
    def setUpClass(cls):
        cls.env = cls.test_folder / "test_pickle"
        cls.env_object = cls.env / "test"
        cls.env.mkdir()

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.env)

    def setUp(self):
        self.probe = TaskProbe()

    def test_singleton(self):
        probe2 = TaskProbe()
        self.assertEqual(id(probe2), id(self.probe))

    def test_set_start(self):
        self.probe.set_start(WorkflowTasks.LAUNCH)
        rec = self.probe.get_task_record(WorkflowTasks.LAUNCH)
        self.assertIsNotNone(rec.start)
        self.assertIsNone(rec.end)

    def test_set_end(self):
        self.probe.set_end(WorkflowTasks.LAUNCH)
        rec = self.probe.get_task_record(WorkflowTasks.LAUNCH)
        self.assertIsNone(rec.start)
        self.assertIsNotNone(rec.end)

    def test_check_record(self):
        pass

    # TODO: test __setstate__
    def test_setstate(self):
        pass
