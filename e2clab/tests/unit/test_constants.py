from unittest.mock import patch

from e2clab.constants.default import load_def, prefix
from e2clab.tests.unit import TestE2cLab


class TestConstants(TestE2cLab):
    def test_prefix(self):
        self.assertEqual("E2C_TEST", prefix("TEST"))

    def test_load_def(self):
        with patch.dict("os.environ", E2C_TEST="test"):
            val = load_def("TEST", "not")
            self.assertEqual(val, "test")
