#! /bin/bash

LOG_FILENAME="e2clab.log"
LOG_ERR_FILENAME="e2clab.err"

assertion_passed=true

assert() {
    if ! eval "$1"; then
        echo "Assertion failed: $2"
        assertion_passed=false
        return 1
    fi
}

# Function to get disk usage of a file or folder
get_disk_usage() {

    # Check if the path exists
    if [ -e "$1" ]; then
        # Get the disk usage using 'du' and return it
        local usage=$(du -sk "$1" 2>/dev/null | cut -f1)
        echo "$usage"
    else
        echo "Error: $path does not exist."
        return 1
    fi
}

# Check logfiles
check_log_files() {
    local path="$1"

    # Check if the path exists
    if [ -e "$path" ]; then
        (
            cd "$path"
            assert "[[ -e $LOG_FILENAME ]]" "Missing logfile $LOG_FILENAME in $1"
            if [[ $? -eq 0 ]]; then
                log_size="$(get_disk_usage $LOG_FILENAME)"
                assert "[[ $log_size -ne 0 ]]" "Empty $LOG_FILENAME"
            fi
            assert "[[ -e $LOG_ERR_FILENAME ]]" "Missing logfile $LOG_ERR_FILENAME in $1"
            if [[ $? -eq 0 ]]; then
                err_size="$(get_disk_usage $LOG_ERR_FILENAME)"
                assert "[[ $err_size -eq 0 ]]" "Non-empty $LOG_ERR_FILENAME"
            fi
        )
    fi
}

check_log_file() {
    local path="$1"

    # Check if the path exists
    if [ -e "$path" ]; then
        (
            cd "$path"
            assert "[[ -e $LOG_FILENAME ]]" "Missing logfile $LOG_FILENAME in $1"
            if [[ $? -eq 0 ]]; then
                log_size="$(get_disk_usage $LOG_FILENAME)"
                assert "[[ $log_size -ne 0 ]]" "Empty $LOG_FILENAME"
            fi
        )
    fi
}
