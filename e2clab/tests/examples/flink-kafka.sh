#!/bin/bash

# CI purpose

source "$(dirname "$0")/utils.sh"
if [[ $? -eq 0 ]]; then
    echo "Correctly sourced utils.sh"
else
    echo "Failed sourcing utils.sh"
fi

# Cloning and adding services
SERVICES_DIR=/tmp/services

rm -rf $SERVICES_DIR

git clone https://gitlab.inria.fr/E2Clab/user-defined-services.git $SERVICES_DIR

e2clab services remove Flink
e2clab services add $SERVICES_DIR/Flink.py
if [[ $? -ne 0 ]]; then
    echo "FAILED IMPORTING Flink service"
    exit 1
fi

e2clab services remove Kafka
e2clab services add $SERVICES_DIR/Kafka.py
if [[ $? -ne 0 ]]; then
    echo "FAILED IMPORTING Kafka service"
    exit 1
fi

rm -rf $SERVICES_DIR

# Defining experiment variables
echo "MOVING TO EXPERIMENT FOLDER"
cd examples/flink-kafka || exit 1
export E2C_SCENARIO_DIR="getting_started/"
export E2C_ARTIFACTS_DIR="artifacts/"

# Running experiment
e2clab deploy --repeat 1 --duration 60
e2clab destroy

# BEGIN CHECK ASSERTIONS ON EXPERIMENT OUTPUT

# cd to output dir
OUTPUT_DIR="$(e2clab -e get-output-dir)"

if [[ ! -d "$OUTPUT_DIR" ]]; then
    echo "NO VALID OUTPUT DIR"
    exit 1
fi

cd "$OUTPUT_DIR" || exit 1

expected_files=($LOG_FILENAME $LOG_ERR_FILENAME "layers_services-validate.yaml" "workflow-validate.out")
expected_folders=("monitoring-data" "network-validate")
expected_subfolder_count=(6 1) # NOTE: 6 monitored hosts in exeriment
expected_subfiles_count=(0 0)

check_log_files "."

for file in "${expected_files[@]}"; do
    assert "[[ -e $file ]]" "Expected output file: $file does not exist in $(pwd)"
done

for i in "${!expected_folders[@]}"; do
    folder="${expected_folders[$i]}"
    if [[ ! -e $folder ]]; then
        echo "Expected output file: $folder does not exist in $(pwd)"
    fi
    (
        cd "$folder" || exit

        mapfile -d '' folders < <(find . -maxdepth 1 -mindepth 1 -type d -print0)
        sub_folders_count=${#folders[@]}

        readarray -d '' files < <(find . -maxdepth 1 -mindepth 1 -type f -print0)
        sub_files_count=${#files[@]}

        expected="${expected_subfolder_count[$i]}"
        assert "[[ $sub_folders_count -eq $expected ]]" "Expected: $expected, got: $sub_folders_count subfolders in $folder"

        expected="${expected_subfiles_count[$i]}"
        assert "[[ $sub_files_count -eq $expected ]]" "Expected: $expected, got: $sub_files_count files in $folder"

        for f in "${folders[@]}"; do
            size="$(du -sk $f | awk '{print $1}')"
            assert "[[ $size -ne 0 ]]" "Empty folder $f in $folder"
        done
    )
done

if ! $assertion_passed; then
    echo "Assertions failed"
    exit 1
fi

echo "Assertions passed"
