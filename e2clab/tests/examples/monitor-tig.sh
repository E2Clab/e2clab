#!/bin/bash

# CI purpose

source "$(dirname "$0")/utils.sh"
if [[ $? -eq 0 ]]; then
  echo "Correctly sourced utils.sh"
else
  echo "Failed sourcing utils.sh"
fi

# Defining experiment variables
EXPERIMENT_FOLDER="examples/monitoring-tig-g5k"
echo "MOVING TO EXPERIMENT FOLDER: $EXPERIMENT_FOLDER"
cd "$EXPERIMENT_FOLDER" || exit 1

export E2C_SCENARIO_DIR="./"
export E2C_ARTIFACTS_DIR="./"

# Running experiment
e2clab layers-services
e2clab workflow . prepare
e2clab workflow . launch
e2clab finalize
e2clab destroy

# BEGIN CHECK ASSERTIONS ON EXPERIMENT OUTPUT

# cd to output dir
OUTPUT_DIR="$(e2clab -e get-output-dir)"

if [[ ! -d "$OUTPUT_DIR" ]]; then
  echo "NO VALID OUTPUT DIR"
  exit 1
fi

cd "$OUTPUT_DIR" || exit 1

expected_files=("layers_services-validate.yaml" "workflow-validate.out")
expected_folders=("monitoring-data")
expected_subfolder_count=(O)
expected_subfiles_count=(1) # NOTE: One compressed database file

check_log_files "."

for file in "${expected_files[@]}"; do
  assert "[[ -e $file ]]" "Expected output file: $file does not exist in $(pwd)"
done

for i in "${!expected_folders[@]}"; do
  folder="${expected_folders[$i]}"
  if [[ ! -e $folder ]]; then
    echo "Expected output file: $folder does not exist in $(pwd)"
  fi
  (
    cd "$folder" || exit

    mapfile -d '' folders < <(find . -maxdepth 1 -mindepth 1 -type d -print0)
    sub_folders_count=${#folders[@]}

    readarray -d '' files < <(find . -maxdepth 1 -mindepth 1 -type f -print0)
    sub_files_count=${#files[@]}

    expected="${expected_subfolder_count[$i]}"
    assert "[[ $sub_folders_count -eq $expected ]]" "Expected: $expected, got: $sub_folders_count subfolders in $folder"

    expected="${expected_subfiles_count[$i]}"
    assert "[[ $sub_files_count -eq $expected ]]" "Expected: $expected, got: $sub_files_count files in $folder"

    for f in "${files[@]}"; do
      size="$(du -sk $f | awk '{print $1}')"
      assert "[[ $size -ne 0 ]]" "Empty file $f in $folder"
    done

    for f in "${folders[@]}"; do
      size="$(du -sk $f | awk '{print $1}')"
      assert "[[ $size -ne 0 ]]" "Empty folder $f in $folder"
    done
  )
done

if ! $assertion_passed; then
  echo "Assertions failed"
  exit 1
fi

echo "Assertions passed"
