from e2clab.services.service import Service


class NotDefault3(Service):
    """
    A dummy service for unittest
    """

    def deploy(self):
        return None
